package com.denisloh.typhoonCamera.preferences.widget;

import android.app.AlertDialog;
import android.content.Context;
import android.content.res.TypedArray;
import android.preference.DialogPreference;
import android.util.AttributeSet;
import android.view.Gravity;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.NumberPicker;

import com.denisloh.typhoonCamera.R;

@SuppressWarnings("WeakerAccess")
public class NumberPickerPreference extends DialogPreference {

    private static final int     MIN_VALUE           = 0;
    private static final int     MAX_VALUE           = 100;
    private static final boolean WRAP_SELECTOR_WHEEL = false;

    private int           mSelectedValue;
    private final int     mMinValue;
    private final int     mMaxValue;
    private final boolean mWrapSelectorWheel;
    private NumberPicker  mNumberPicker;

    public NumberPickerPreference(final Context context, final AttributeSet attrs) {
        super(context, attrs);

        final TypedArray a = context.obtainStyledAttributes(attrs, R.styleable.NumberPickerPreference);

        mMinValue = a.getInt(R.styleable.NumberPickerPreference_minValue, MIN_VALUE);
        mMaxValue = a.getInt(R.styleable.NumberPickerPreference_maxValue, MAX_VALUE);
        mWrapSelectorWheel = a.getBoolean(R.styleable.NumberPickerPreference_setWrapSelectorWheel, WRAP_SELECTOR_WHEEL);

        a.recycle();
    }

    @Override
    protected void onSetInitialValue(final boolean restoreValue, final Object defaultValue) {
        final int intDefaultValue = defaultValue instanceof Integer ? (int) defaultValue : mMinValue;
        mSelectedValue = restoreValue ? this.getPersistedInt(intDefaultValue) : intDefaultValue;
        this.updateSummary();
    }

    @Override
    protected Object onGetDefaultValue(final TypedArray a, final int index) {
        return a.getInteger(index, 0);
    }

    @Override
    protected void onPrepareDialogBuilder(final AlertDialog.Builder builder) {
        super.onPrepareDialogBuilder(builder);

        mNumberPicker = new NumberPicker(this.getContext());
        mNumberPicker.setMinValue(mMinValue);
        mNumberPicker.setMaxValue(mMaxValue);
        mNumberPicker.setValue(mSelectedValue);
        mNumberPicker.setWrapSelectorWheel(mWrapSelectorWheel);
        mNumberPicker.setLayoutParams(new LinearLayout.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT));

        final LinearLayout linearLayout = new LinearLayout(this.getContext());
        linearLayout.setLayoutParams(new LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT));
        linearLayout.setGravity(Gravity.CENTER);
        linearLayout.addView(mNumberPicker);

        builder.setView(linearLayout);
    }

    @Override
    protected void onDialogClosed(final boolean positiveResult) {
        super.onDialogClosed(positiveResult);

        if (positiveResult) {
            final int selectedValue = mNumberPicker.getValue();

            if (this.callChangeListener(selectedValue)) {
                mSelectedValue = selectedValue;

                this.updateSummary();
                this.persistInt(mSelectedValue);
            }
        }
    }

    private void updateSummary() {
        this.setSummary(String.valueOf(mSelectedValue));
    }

    public int getValue() {
        return this.mSelectedValue;
    }

}
