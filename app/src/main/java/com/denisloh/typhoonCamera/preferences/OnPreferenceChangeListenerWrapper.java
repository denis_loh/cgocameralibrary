package com.denisloh.typhoonCamera.preferences;

import android.preference.Preference;

import java.util.ArrayList;
import java.util.List;

public class OnPreferenceChangeListenerWrapper implements Preference.OnPreferenceChangeListener {

    private List<Preference.OnPreferenceChangeListener> mListeners = new ArrayList<>();

    public void addOnPreferenceChangeListener(Preference.OnPreferenceChangeListener listener) {
        if (!mListeners.contains(listener)) {
            mListeners.add(listener);
        }
    }

    @Override
    public boolean onPreferenceChange(Preference preference, Object newValue) {
        boolean state = false;
        for (Preference.OnPreferenceChangeListener listener : mListeners) {
            state = listener.onPreferenceChange(preference, newValue);
        }
        return state;
    }

    public static void registerOnPreferenceChangeListener(Preference preference,
                                                          Preference.OnPreferenceChangeListener listener) {
        OnPreferenceChangeListenerWrapper wrapper;

        if (preference.getOnPreferenceChangeListener() instanceof OnPreferenceChangeListenerWrapper) {
            wrapper = (OnPreferenceChangeListenerWrapper) preference.getOnPreferenceChangeListener();
        } else {
            wrapper = new OnPreferenceChangeListenerWrapper();
            preference.setOnPreferenceChangeListener(wrapper);
        }

        wrapper.addOnPreferenceChangeListener(listener);
    }
}
