package com.denisloh.typhoonCamera.preferences;

import android.annotation.TargetApi;
import android.os.Build;
import android.os.Bundle;
import android.preference.ListPreference;

import com.denisloh.cgocamera.CGOCamera;
import com.denisloh.typhoonCamera.R;
import com.denisloh.typhoonCamera.SettingsActivity;

/**
 * This fragment shows notification preferences only. It is used when the
 * activity is showing a two-pane settings UI.
 */
@TargetApi(Build.VERSION_CODES.HONEYCOMB)
public class PhotoPreferenceFragment extends SettingsActivity.BasicPreferenceFragment {
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        addPreferencesFromResource(R.xml.pref_photo);

        ListPreference photoFormatPreference = (ListPreference) findPreference("photo_format");

        String[] entries = new String[] {
                CGOCamera.PhotoFormat.JPG.FormatString,
                CGOCamera.PhotoFormat.DNG.FormatString
        };

        photoFormatPreference.setEntries(entries);
        photoFormatPreference.setEntryValues(entries);
        if (photoFormatPreference.getValue() == null) {
            photoFormatPreference.setValue(entries[0]);
        }

        bindPreferenceSummaryToValue(findPreference("photo_sharpness"));
        bindPreferenceSummaryToValue(photoFormatPreference);
    }
}
