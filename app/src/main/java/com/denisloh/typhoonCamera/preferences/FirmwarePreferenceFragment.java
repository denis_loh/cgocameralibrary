package com.denisloh.typhoonCamera.preferences;

import android.annotation.TargetApi;
import android.os.Build;
import android.os.Bundle;
import android.os.Message;
import android.util.Log;
import android.widget.Toast;

import com.denisloh.cgocamera.CGOCamera;
import com.denisloh.cgocamera.CameraParameters;
import com.denisloh.cgocamera.utils.WeakHandler;
import com.denisloh.typhoonCamera.R;
import com.denisloh.typhoonCamera.SettingsActivity;

import static com.denisloh.cgocamera.CGOCamera.KEY_EXCEPTION;
import static com.denisloh.cgocamera.CGOCamera.NO_ERROR;

/**
 * This fragment shows data and sync preferences only. It is used when the
 * activity is showing a two-pane settings UI.
 */
@TargetApi(Build.VERSION_CODES.HONEYCOMB)
public class FirmwarePreferenceFragment extends SettingsActivity.BasicPreferenceFragment {

    private static final int PREFERENCE_SCREEN = R.xml.pref_firmware;
    private static final String TAG = FirmwarePreferenceFragment.class.getSimpleName();

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        addPreferencesFromResource(PREFERENCE_SCREEN);

        final CGOCamera cgoCamera = getCamera();

        refresh();

        if (cgoCamera != null && cgoCamera.supportsCommand(CGOCamera.Command.GET_FIRMWARE_INFO)) {
            cgoCamera.getFirmwareInfo(new FirmwareInfoHandler(this));
        } else {
            Toast.makeText(
                    getActivity(),
                    getString(R.string.pref_firmware_refresh_failed),
                    Toast.LENGTH_SHORT)
                    .show();
        }

    }

    private void refresh() {
        final CGOCamera cgoCamera = getCamera();
        if (cgoCamera != null) {
            CameraParameters.FirmwareInfo info = cgoCamera.getCurrentStatus().Firmware;

            setSummary("firmware_brand", info.getBrand());
            setSummary("firmware_model", info.getModel());
            setSummary("firmware_version", info.getVersion());
            setSummary("firmware_apiversion", info.getApiVersion());
            setSummary("firmware_versiondate", info.getVersionDate());
            setSummary("firmware_sensorchip", info.getSensorChip());
        }
    }

    private static class FirmwareInfoHandler extends WeakHandler<FirmwarePreferenceFragment> {

        /**
         * Creates a new weak handler of type T
         *
         * @param owner The owner object which should be referenced
         */
        public FirmwareInfoHandler(FirmwarePreferenceFragment owner) {
            super(owner);
        }

        @Override
        public void handleMessage(Message msg) {
            if (msg.what == CGOCamera.Command.GET_FIRMWARE_INFO.ordinal()) {
                if (msg.arg1 == NO_ERROR) {
                    getOwner().refresh();
                } else {
                    Bundle b = msg.getData();
                    Throwable error = null;
                    if (b.containsKey(KEY_EXCEPTION)) {
                        error = (Throwable) b.getSerializable(KEY_EXCEPTION);
                    }
                    Log.w(TAG, "Failed to load firmware info", error);

                    Toast.makeText(
                            getOwner().getActivity(),
                            getOwner().getString(R.string.pref_firmware_refresh_failed),
                            Toast.LENGTH_SHORT)
                            .show();
                }
            }
        }
    }

}
