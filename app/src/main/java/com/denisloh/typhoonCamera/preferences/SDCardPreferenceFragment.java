package com.denisloh.typhoonCamera.preferences;

import android.annotation.TargetApi;
import android.os.Build;
import android.os.Bundle;
import android.os.Message;
import android.support.design.widget.Snackbar;
import android.util.Log;

import com.denisloh.cgocamera.CGOCamera;
import com.denisloh.cgocamera.utils.WeakHandler;
import com.denisloh.typhoonCamera.R;
import com.denisloh.typhoonCamera.SettingsActivity;
import com.denisloh.typhoonCamera.preferences.widget.DialogPreference;
import com.denisloh.typhoonCamera.preferences.widget.MemoryPreference;

import static com.denisloh.cgocamera.CGOCamera.KEY_EXCEPTION;
import static com.denisloh.cgocamera.CGOCamera.NO_ERROR;

@TargetApi(Build.VERSION_CODES.HONEYCOMB)
public class SDCardPreferenceFragment extends SettingsActivity.BasicPreferenceFragment {

    private static final String TAG = SDCardPreferenceFragment.class.getSimpleName();

    private static final class SDCardHandler extends WeakHandler<SDCardPreferenceFragment> {

        public SDCardHandler(SDCardPreferenceFragment owner) {
            super(owner);
        }

        @Override
        public void handleMessage(Message msg) {
            SDCardPreferenceFragment that = getOwner();

            if (msg.what == CGOCamera.Command.FORMAT_SDCARD.ordinal()) {
                if (msg.arg1 == NO_ERROR) {
                    if (that.getView() != null) {
                        Snackbar.make(
                                that.getView().getRootView(),
                                that.getString(R.string.pref_sdcard_format_card_success),
                                Snackbar.LENGTH_LONG)
                                .show();
                    }
                } else {
                    Bundle b = msg.getData();
                    Throwable error = null;
                    if (b.containsKey(KEY_EXCEPTION)) {
                        error = (Throwable) b.getSerializable(KEY_EXCEPTION);
                    }
                    Log.w(TAG, "Failed to format SD card", error);

                    if (that.getView() != null) {
                        Snackbar.make(
                                that.getView().getRootView(),
                                that.getString(R.string.pref_sdcard_format_card_failed),
                                Snackbar.LENGTH_LONG)
                                .show();
                    }
                }
            }
        }
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        addPreferencesFromResource(R.xml.pref_sdcard);

        final CGOCamera cgoCamera = getCamera();

        MemoryPreference memoryPreference = (MemoryPreference) findPreference("memory_usage");
        if (memoryPreference != null) {
            long totalMemory = cgoCamera.getCurrentStatus().getSDCardTotalMemory() * 1024;
            long freeMemory = cgoCamera.getCurrentStatus().getSDCardFreeMemory() * 1024;
            memoryPreference.setTotalMemory(totalMemory);
            memoryPreference.setUsedMemory(totalMemory - freeMemory);
        }

        DialogPreference dialogPreference = (DialogPreference) findPreference("memory_format_card");
        dialogPreference.setOnDialogClosedCallback(new DialogPreference.OnDialogClosedCallback() {
            @Override
            public void onDialogClosed(boolean positiveResult) {
                if (positiveResult) {
                    cgoCamera.formatSDCard(new SDCardHandler(SDCardPreferenceFragment.this));
                }
            }
        });
    }

}
