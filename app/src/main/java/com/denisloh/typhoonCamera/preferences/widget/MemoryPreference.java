package com.denisloh.typhoonCamera.preferences.widget;

import android.content.Context;
import android.preference.Preference;
import android.text.SpannableString;
import android.text.style.RelativeSizeSpan;
import android.util.AttributeSet;
import android.view.View;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.denisloh.typhoonCamera.R;

import java.util.Locale;

public class MemoryPreference extends Preference {

    private long mUsedMemory;
    private long mTotalMemory;

    public MemoryPreference(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);

        setWidgetLayoutResource(R.layout.preference_memory);
    }

    public MemoryPreference(Context context, AttributeSet attrs) {
        this(context, attrs, 0);
    }

    public MemoryPreference(Context context) {
        this(context, null);
    }

    public void setUsedMemory(long usedMemory) {
        this.mUsedMemory = usedMemory;
    }

    public void setTotalMemory(long totalMemory) {
        this.mTotalMemory = totalMemory;
    }

    @Override
    protected void onBindView(View view) {
        super.onBindView(view);

        TextView memoryUsed = (TextView) view.findViewById(R.id.memory_used);
        TextView memoryTotal = (TextView) view.findViewById(R.id.memory_total);

        ProgressBar progressBar = (ProgressBar) view.findViewById(R.id.memory_progressBar);

        double progress = 0;

        if (mTotalMemory > 0) {
            progress = 100 * mUsedMemory / mTotalMemory;
        }

        String usedMemoryString = humanReadableByteCount(mUsedMemory, true);
        SpannableString spannableString = new SpannableString(usedMemoryString);
        spannableString.setSpan(new RelativeSizeSpan(0.5f), usedMemoryString.indexOf(' '), usedMemoryString.length(), 0);

        memoryUsed.setText(spannableString);
        memoryTotal.setText(view.getContext().getString(R.string.pref_memory_total, humanReadableByteCount(mTotalMemory, true)));

        progressBar.setProgress((int) progress);
    }

    private String humanReadableByteCount(long bytes, boolean si) {
        int unit = si ? 1000 : 1024;
        if (bytes < unit) return bytes + " B";
        int exp = (int) (Math.log(bytes) / Math.log(unit));
        String pre = (si ? "kMGTPE" : "KMGTPE").charAt(exp-1) + (si ? "" : "i");
        return String.format(Locale.getDefault(), "%.2f %sB", ((double) bytes) / Math.pow(unit, exp), pre);
    }
}
