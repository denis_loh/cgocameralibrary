package com.denisloh.typhoonCamera.preferences;

import android.annotation.TargetApi;
import android.os.Build;
import android.os.Bundle;
import android.preference.ListPreference;
import android.preference.Preference;

import com.denisloh.cgocamera.CGOCamera;
import com.denisloh.typhoonCamera.R;
import com.denisloh.typhoonCamera.SettingsActivity;

import java.util.ArrayList;
import java.util.List;

/**
 * This fragment shows general preferences only. It is used when the
 * activity is showing a two-pane settings UI.
 */
@TargetApi(Build.VERSION_CODES.HONEYCOMB)
public class VideoPreferenceFragment extends SettingsActivity.BasicPreferenceFragment {

    private ListPreference mFrameRatePref;
    private List<CGOCamera.VideoMode> mVideoModes;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        addPreferencesFromResource(R.xml.pref_video);

        if (getCamera() != null) {
            mVideoModes = getCamera().getVideoModes();
        }

        ListPreference mResolutionPref = (ListPreference) findPreference("video_resolution");
        mFrameRatePref = (ListPreference) findPreference("video_framerate");

        List<String> resolutions = getResolutions(mVideoModes);

        CharSequence[] resArray;
        if (resolutions.size() > 0) {
            resArray = resolutions.toArray(new CharSequence[resolutions.size()]);
            mResolutionPref.setDefaultValue(resolutions.get(0));

            if (mResolutionPref.getValue() == null ||
                    mResolutionPref.findIndexOfValue(mResolutionPref.getValue()) == -1) {
                mResolutionPref.setValue(resolutions.get(0));
            }
        } else {
            resArray = new CharSequence[0];
        }

        mResolutionPref.setEntries(resArray);
        mResolutionPref.setEntryValues(resArray);

        OnPreferenceChangeListenerWrapper.
                registerOnPreferenceChangeListener(mResolutionPref, new Preference.OnPreferenceChangeListener() {
                    @Override
                    public boolean onPreferenceChange(Preference preference, Object newValue) {
                        updateFrameRatePref((String) newValue);
                        return true;
                    }
                });

        updateFrameRatePref(mResolutionPref.getValue());

        bindPreferenceSummaryToValue(mResolutionPref);
        bindPreferenceSummaryToValue(mFrameRatePref);
    }

    private void updateFrameRatePref(String value) {
        List<String> frameRates = getFrameRates(mVideoModes, value);
        CharSequence[] fpsArray = frameRates.toArray(new CharSequence[frameRates.size()]);
        mFrameRatePref.setEntries(fpsArray);
        mFrameRatePref.setEntryValues(fpsArray);

        if (frameRates.size() > 0) {
            String initialValue = frameRates.get(0);
            mFrameRatePref.setDefaultValue(initialValue);

            if (mFrameRatePref.getValue() == null ||
                    mFrameRatePref.findIndexOfValue(mFrameRatePref.getValue()) == -1) {
                mFrameRatePref.setValue(initialValue);

                Preference.OnPreferenceChangeListener listener =
                        mFrameRatePref.getOnPreferenceChangeListener();
                if (listener != null) {
                    listener.onPreferenceChange(mFrameRatePref, initialValue);
                }
            }
        }
    }

    private List<String> getResolutions(List<CGOCamera.VideoMode> videoModes) {
        List<String> resolutions = new ArrayList<>();
        if (videoModes != null) {
            for (CGOCamera.VideoMode mode : videoModes) {
                if (!resolutions.contains(mode.Resolution)) {
                    resolutions.add(mode.Resolution);
                }
            }
        }

        return resolutions;
    }

    private List<String> getFrameRates(List<CGOCamera.VideoMode> videoModes, String resolution) {
        List<String> frameRates = new ArrayList<>();
        if (videoModes != null) {
            for (CGOCamera.VideoMode mode : videoModes) {
                if (mode.Resolution.equals(resolution)) {
                    frameRates.add(mode.FrameRate);
                }
            }
        }

        return frameRates;
    }

}
