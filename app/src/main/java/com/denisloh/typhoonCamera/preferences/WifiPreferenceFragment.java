package com.denisloh.typhoonCamera.preferences;

import android.os.Bundle;

import com.denisloh.cgocamera.CGOCamera;
import com.denisloh.typhoonCamera.R;
import com.denisloh.typhoonCamera.SettingsActivity;

public class WifiPreferenceFragment extends SettingsActivity.BasicPreferenceFragment {

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        addPreferencesFromResource(R.xml.pref_wifi);

        CGOCamera cgoCamera = getCamera();

        bindPreferenceSummaryToValue(findPreference("wifi_auto_connect"));

        setSummary("wifi_link_speed",
                (cgoCamera != null) ?
                        cgoCamera.getCurrentStatus().getSpeedRate() :
                        getString(R.string.not_connected)
        );

    }

}
