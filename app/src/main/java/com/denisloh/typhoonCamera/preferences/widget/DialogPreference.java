package com.denisloh.typhoonCamera.preferences.widget;

import android.content.Context;
import android.util.AttributeSet;

public class DialogPreference extends android.preference.DialogPreference {

    public interface OnDialogClosedCallback {
        void onDialogClosed(boolean positiveResult);
    }

    private OnDialogClosedCallback mClosedCallback;

    public DialogPreference(Context context, AttributeSet attrs) {
        super(context, attrs, 0);
    }

    public DialogPreference(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
    }

    public void setOnDialogClosedCallback(OnDialogClosedCallback callback) {
        mClosedCallback = callback;
    }

    @Override
    protected void onDialogClosed(boolean positiveResult) {
        super.onDialogClosed(positiveResult);

        if (mClosedCallback != null) {
            mClosedCallback.onDialogClosed(positiveResult);
        }
    }
}
