package com.denisloh.typhoonCamera;

import android.app.Fragment;
import android.app.FragmentManager;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.ListPreference;
import android.preference.Preference;
import android.preference.PreferenceCategory;
import android.preference.PreferenceFragment;
import android.preference.PreferenceGroup;
import android.preference.PreferenceManager;
import android.preference.PreferenceScreen;
import android.provider.Settings;
import android.support.annotation.NonNull;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;

import com.denisloh.cgocamera.CGOCamera;
import com.denisloh.cgocamera.CGOCameraManager;
import com.denisloh.cgocamera.cameras.MK58Camera;
import com.denisloh.typhoonCamera.preferences.FirmwarePreferenceFragment;
import com.denisloh.typhoonCamera.preferences.OnPreferenceChangeListenerWrapper;
import com.denisloh.typhoonCamera.preferences.SDCardPreferenceFragment;
import com.denisloh.typhoonCamera.preferences.VideoPreferenceFragment;
import com.denisloh.typhoonCamera.preferences.PhotoPreferenceFragment;
import com.denisloh.typhoonCamera.preferences.WifiPreferenceFragment;

import java.lang.reflect.Constructor;

public class SettingsActivity extends AppCompatActivity {

    private static final String TAG = SettingsActivity.class.getSimpleName();

    private static final String EXTRA_FRAGMENT = "fragment";

    //region Preference Change Listener

    /**
     * A preference value change listener that updates the preference's summary
     * to reflect its new value.
     */
    private static Preference.OnPreferenceChangeListener sBindPreferenceSummaryToValueListener = new Preference.OnPreferenceChangeListener() {
        @Override
        public boolean onPreferenceChange(Preference preference, Object value) {
            String stringValue = value.toString();

            if (preference instanceof ListPreference) {
                // For list preferences, look up the correct display value in
                // the preference's 'entries' list.
                ListPreference listPreference = (ListPreference) preference;
                int index = listPreference.findIndexOfValue(stringValue);

                // Set the summary to reflect the new value.
                preference.setSummary(
                        index >= 0
                                ? listPreference.getEntries()[index]
                                : null);

            } else {
                // For all other preferences, set the summary to the value's
                // simple string representation.
                preference.setSummary(stringValue);
            }
            return true;
        }
    };

    /**
     * Binds a preference's summary to its value. More specifically, when the
     * preference's value is changed, its summary (line of text below the
     * preference title) is updated to reflect the value. The summary is also
     * immediately updated upon calling this method. The exact display format is
     * dependent on the type of preference.
     *
     * @see #sBindPreferenceSummaryToValueListener
     */
    private static void bindPreferenceSummaryToValue(Preference preference) {
        // Set the listener to watch for value changes.

        OnPreferenceChangeListenerWrapper.registerOnPreferenceChangeListener(preference, sBindPreferenceSummaryToValueListener);

        SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(preference.getContext());
        Object value = prefs.getAll().get(preference.getKey());

        // Trigger the listener immediately with the preference's
        // current value.
        sBindPreferenceSummaryToValueListener.onPreferenceChange(preference, value != null ? value.toString() : "");

    }
    //endregion

    //region Preference Fragment
    public static abstract class BasicPreferenceFragment extends PreferenceFragment {

        private CGOCamera mCGOCamera;

        protected void bindPreferenceSummaryToValue(Preference preference) {
            SettingsActivity.bindPreferenceSummaryToValue(preference);
        }

        protected CGOCamera getCamera() {
            return mCGOCamera;
        }

        @Override
        public void onCreate(Bundle savedInstanceState) {
            super.onCreate(savedInstanceState);
            setHasOptionsMenu(true);

            mCGOCamera = CGOCameraManager.getCamera();
        }

        @Override
        public boolean onOptionsItemSelected(MenuItem item) {
            int id = item.getItemId();
            if (id == android.R.id.home) {
                startActivity(new Intent(getActivity(), SettingsActivity.class));
                return true;
            }
            return super.onOptionsItemSelected(item);
        }

        protected void setSummary(String key, String value) {
            Preference preference = findPreference(key);
            if (preference != null) {
                preference.setSummary(value);
            }
        }

    }

    public static final class OverviewFragment extends PreferenceFragment {

        private CGOCamera mCamera;
        private PreferenceCategory mCameraPreferences;

        @Override
        public void onCreate(Bundle savedInstanceState) {
            super.onCreate(savedInstanceState);
            addPreferencesFromResource(R.xml.pref_headers);

            mCamera = CGOCameraManager.getCamera();

            initPreferences(getPreferenceScreen());
        }

        @Override
        public void onResume() {
            super.onResume();

            if (mCamera == null || !mCamera.isConnected()) {
                Log.d(TAG, "Camera ("+ (mCamera == null ? "?" : mCamera.getName()) + ") is not set or not connected");
                ((SettingsActivity)getActivity()).showCameraNotConnectedSnackBar();
            }
        }

        private void initPreferences(PreferenceScreen preferences) {

            PreferenceCategory generalPreferences =
                    (PreferenceCategory) preferences.findPreference("pref_key_cat_general");

            // Show settings if camera is available
            if (mCamera != null) {

                // Show settings if it is not a MK58 downlink
                if (mCamera.getId() != MK58Camera.ID) {
                    mCameraPreferences = createCameraPreferences(preferences);

                    // Disable settings if camera is not connected.
                    if (!mCamera.isConnected()) {
                        for (int i = 0; i < mCameraPreferences.getPreferenceCount(); ++i) {
                            mCameraPreferences.getPreference(i).setEnabled(false);
                        }
                    }
                }

                generalPreferences.addPreference(
                        createPreferenceCategory(
                                R.string.pref_header_firmware_info,
                                R.drawable.ic_info_black_24dp,
                                "com.denisloh.typhoonCamera.preferences.FirmwarePreferenceFragment"));
            }

            initOnPreferenceClickListener(preferences);
        }

        private void initOnPreferenceClickListener(Preference preference) {
            if (preference instanceof PreferenceGroup) {
                PreferenceGroup preferences = (PreferenceGroup) preference;
                for (int i = 0; i < preferences.getPreferenceCount(); ++i) {
                    initOnPreferenceClickListener(preferences.getPreference(i));
                }
            } else {
                final String fragment = preference.getFragment();
                if (fragment != null && isValidFragment(fragment)) {
                    preference.setOnPreferenceClickListener(new Preference.OnPreferenceClickListener() {
                        @Override
                        public boolean onPreferenceClick(Preference preference) {
                            Intent intent = new Intent(OverviewFragment.this.getActivity(), SettingsActivity.class);
                            intent.putExtra(EXTRA_FRAGMENT, fragment);

                            startActivity(intent);
                            return true;
                        }
                    });
                }
            }
        }

        private PreferenceCategory createCameraPreferences(PreferenceScreen preferenceScreen) {
            PreferenceCategory cameraPreferences = new PreferenceCategory(this.getActivity());
            cameraPreferences.setTitle(R.string.pref_category_camera);
            cameraPreferences.setKey("pref_key_cat_camera");

            // first item
            cameraPreferences.setOrder(-1);

            preferenceScreen.addPreference(cameraPreferences);

            cameraPreferences.addPreference(
                    createPreferenceCategory(
                            R.string.pref_header_video,
                            R.drawable.ic_videocam_black_24dp,
                            "com.denisloh.typhoonCamera.preferences.VideoPreferenceFragment"));

            cameraPreferences.addPreference(
                    createPreferenceCategory(
                            R.string.pref_header_photo,
                            R.drawable.ic_photo_camera_black_24dp,
                            "com.denisloh.typhoonCamera.preferences.PhotoPreferenceFragment"));

            cameraPreferences.addPreference(
                    createPreferenceCategory(
                            R.string.pref_header_sd_card,
                            R.drawable.ic_sd_card_black_24dp,
                            "com.denisloh.typhoonCamera.preferences.SDCardPreferenceFragment"));

            return cameraPreferences;
        }

        private Preference createPreferenceCategory(
                final int titleRes,
                final int iconRes,
                final String fragment)
        {
            return createPreferenceCategory(titleRes, iconRes, fragment, Preference.DEFAULT_ORDER);
        }

        private Preference createPreferenceCategory(
                final int titleRes,
                final int iconRes,
                final String fragment,
                final int order)
        {
            Preference preference = new Preference(this.getActivity());
            preference.setTitle(titleRes);
            preference.setIcon(iconRes);
            preference.setFragment(fragment);
            preference.setOrder(order);

            return preference;
        }

        private boolean isValidFragment(String fragmentName) {
            return FirmwarePreferenceFragment.class.getName().equals(fragmentName) ||
                    PhotoPreferenceFragment.class.getName().equals(fragmentName) ||
                    VideoPreferenceFragment.class.getName().equals(fragmentName) ||
                    SDCardPreferenceFragment.class.getName().equals(fragmentName)||
                    WifiPreferenceFragment.class.getName().equals(fragmentName);
        }

    }
    //endregion

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_settings);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        if (getSupportActionBar() != null) {
            getSupportActionBar().setTitle(R.string.title_activity_settings);
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        }

        if (savedInstanceState == null) {
            String fragmentClass = getIntent().getStringExtra(EXTRA_FRAGMENT);
            Fragment fragment;

            if (fragmentClass != null) {
                fragment = getFragment(fragmentClass);

            } else {
                fragment = new OverviewFragment();
            }

            FragmentManager fragmentManager = getFragmentManager();
            fragmentManager
                    .beginTransaction()
                    .replace(R.id.settings_fragment, fragment)
                    .commit();
        }
    }

    @Override
    public void finish() {
        super.finish();
        setResult(RESULT_OK);
    }

    private Fragment getFragment(@NonNull String fragmentString) {

        try {
            Class<?> cls = Class.forName(fragmentString);
            Constructor<?> constructor = cls.getConstructor();
            return (Fragment) constructor.newInstance();
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    private void showCameraNotConnectedSnackBar() {
        Snackbar.make(findViewById(android.R.id.content), R.string.not_connected_to_cam, Snackbar.LENGTH_INDEFINITE)
                .setAction(R.string.wifi_settings, new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        startActivity(new Intent(Settings.ACTION_WIFI_SETTINGS));
                    }
                })
                .show();
    }

}
