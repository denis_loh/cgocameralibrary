package com.denisloh.typhoonCamera;

import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.os.Parcelable;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.ActionMenuView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Toast;

import com.denisloh.cgocamera.CGOCamera;
import com.denisloh.cgocamera.CGOCameraManager;
import com.denisloh.cgocamera.CameraParameters;
import com.denisloh.cgocamera.cameras.CGO2Camera;
import com.denisloh.cgocamera.cameras.MK58Camera;
import com.denisloh.cgocamera.utils.WeakHandler;
import com.denisloh.cgocamera.widget.VideoView;

import java.util.List;

import static com.denisloh.cgocamera.CGOCamera.*;

public class MainActivity extends AppCompatActivity implements CGOCameraManager.OnCameraStateChangedListener {

    private static final String TAG = MainActivity.class.getSimpleName();

    private static final int REQUEST_SETTINGS = 3;

    private CGOCameraManager mCGOCameraManager;
    private CGOCamera mCamera;
    private Handler mHandler = new CameraHandler(this);


    /** Views **/
    private VideoView mVideoView;
    private ActionMenuView mActionMenu;
    private FloatingActionButton mShutterReleaseButton;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_main);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        if (getSupportActionBar() != null) {
            getSupportActionBar().setTitle("");
        }

        mActionMenu = (ActionMenuView) toolbar.findViewById(R.id.action_bar);
        mActionMenu.setOnMenuItemClickListener(new ActionMenuView.OnMenuItemClickListener() {
            @Override
            public boolean onMenuItemClick(MenuItem menuItem) {
                return onOptionsItemSelected(menuItem);
            }
        });

        mCGOCameraManager = CGOCameraManager.getInstance(this);

        mShutterReleaseButton = (FloatingActionButton) findViewById(R.id.fab);
        mVideoView = (VideoView) findViewById(R.id.videoView);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_others, menu);
        getMenuInflater().inflate(R.menu.menu_main, mActionMenu.getMenu());

        return true;
    }

    @Override
    public boolean onPrepareOptionsMenu(Menu menu) {
        updateMenu();

        return super.onPrepareOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            startActivityForResult(new Intent(this, SettingsActivity.class), REQUEST_SETTINGS);
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    protected void onStart() {
        super.onStart();

        mCamera = CGOCameraManager.getCamera();
    }

    @Override
    protected void onResume() {
        super.onResume();

        toggleHideyBar();

        mCGOCameraManager.registerOnCameraConnectedListener(this);

        if (mCamera != null) {
            if (mCamera.isConnected()) {
                Log.d(TAG, "init camera");
                mCamera.initCamera(mHandler);
            } else {
                mCamera.connect();
            }
        } else {
            mCGOCameraManager.connect();
        }

        updateUI();
    }

    @Override
    protected void onPause() {
        super.onPause();

        if (mVideoView != null) {
            mVideoView.stopPlayback();
        }

        mCGOCameraManager.unregisterOnCameraConnectedListener(this);
    }

    @Override
    protected void onStop() {
        super.onStop();

        if (mVideoView != null) {
            mVideoView.stopPlayback();
            mVideoView.release(true);
        }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();

        mCGOCameraManager.disconnect();
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        switch (requestCode) {
            case REQUEST_SETTINGS:
                switch (resultCode) {
                    case RESULT_OK:
                        // All required changes were successfully made
                        onUpdateCameraSettings();
                        break;
                    default:
                        break;
                }
                break;
        }

        super.onActivityResult(requestCode, resultCode, data);
    }

    private void onUpdateCameraSettings() {

    }

    @Override
    public void onCamerasDiscovered(List<String> SSIDs) {
        // nothing
    }

    @Override
    public void onConnected(CGOCamera camera) {
        mCamera = camera;
        mCamera.initCamera(mHandler);

        updateUI();
    }

    @Override
    public void onConnectionFailed(String SSID) {
        Log.d(TAG, "Connection failed");
        Toast.makeText(this, getString(R.string.error_wifi_connection_failed, SSID), Toast.LENGTH_LONG).show();
    }

    @Override
    public void onDisconnected() {
        //
    }

    private static class CameraHandler extends WeakHandler<MainActivity> {

        /**
         * Creates a new weak handler of type MainActivity
         *
         * @param owner The owner object which should be referenced
         */
        CameraHandler(MainActivity owner) {
            super(owner);
        }

        @Override
        public void handleMessage(Message msg) {
            MainActivity activity = getOwner();
            if (activity == null) {
                return;
            }

            Bundle b = msg.getData();
            Parcelable parcelable = null;
            Throwable error = null;
            if (msg.arg1 == NO_ERROR) {
                if (b.containsKey(KEY_DATA)) {
                    parcelable = b.getParcelable(KEY_DATA);
                }
            } else {
                if (b.containsKey(KEY_EXCEPTION)) {
                    error = (Throwable) b.getSerializable(KEY_EXCEPTION);
                }
            }

            switch (Command.values()[msg.what]) {
                case INIT_CAMERA:
                    if (msg.arg1 == NO_ERROR) {
                        activity.mVideoView.setVideoPath(activity.mCamera.getLiveStreamUrl());
                        if (parcelable != null) {
                            activity.updateStatus((CameraParameters) parcelable);
                        }
                        activity.startVideoPlayback();
                    } else {
                        Log.e(TAG, "Error while init camera", error);
                    }
                    break;
                case GET_STATUS:
                    if (msg.arg1 == NO_ERROR) {
                        if (parcelable != null) {
                            activity.updateStatus((CameraParameters) parcelable);
                        }
                    } else {
                        Log.e(TAG, "Error while get camera status", error);
                    }
                    break;
                case START_RECORDING:
                    if (msg.arg1 == NO_ERROR) {
                        Log.d(TAG, "Successfully start recording");
                    } else {
                        Log.e(TAG, "Error while starting recording", error);
                    }
                    break;
                case STOP_RECORDING:
                    if (msg.arg1 == NO_ERROR) {
                        Log.d(TAG, "Successfully stopped recording");
                    } else {
                        Log.e(TAG, "Error while stopping recording", error);
                    }
                    break;
                default:
                    Log.i(TAG, "Unsupported command");
            }
        }
    }

    private void startVideoPlayback() {
        if (mVideoView != null && !mVideoView.isPlaying()) {
            Log.d(TAG, "Starting video");
            mVideoView.start();
        }
    }

    private void updateStatus(CameraParameters parameters) {
        Log.d(TAG, "Status: " + parameters.getStatus());
    }

    /**
     * Detects and toggles immersive mode (also known as "hidey bar" mode).
     */
    private void toggleHideyBar() {

        // The UI options currently enabled are represented by a bitfield.
        // getSystemUiVisibility() gives us that bitfield.
        int newUiOptions = getWindow().getDecorView().getSystemUiVisibility();

        newUiOptions |= View.SYSTEM_UI_FLAG_LAYOUT_STABLE |
                View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN |
                View.SYSTEM_UI_FLAG_LAYOUT_HIDE_NAVIGATION;

        // Navigation bar hiding:  Backwards compatible to ICS.
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.ICE_CREAM_SANDWICH) {
            newUiOptions |= View.SYSTEM_UI_FLAG_HIDE_NAVIGATION;
        }

        // Status bar hiding: Backwards compatible to Jellybean
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN) {
            newUiOptions |= View.SYSTEM_UI_FLAG_FULLSCREEN;
        }

        // Immersive mode: Backward compatible to KitKat.
        // Note that this flag doesn't do anything by itself, it only augments the behavior
        // of HIDE_NAVIGATION and FLAG_FULLSCREEN.  For the purposes of this sample
        // all three flags are being toggled together.
        // Note that there are two immersive mode UI flags, one of which is referred to as "sticky".
        // Sticky immersive mode differs in that it makes the navigation and status bars
        // semi-transparent, and the UI flag does not get cleared when the user interacts with
        // the screen.
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {
            newUiOptions |= View.SYSTEM_UI_FLAG_IMMERSIVE_STICKY;
        }

        getWindow().getDecorView().setSystemUiVisibility(newUiOptions);
    }

    private void updateMenu() {
        MenuItem itemExposure = mActionMenu.getMenu().findItem(R.id.action_exposure);
        MenuItem itemWB = mActionMenu.getMenu().findItem(R.id.action_white_balance);
        MenuItem itemISO = mActionMenu.getMenu().findItem(R.id.action_iso);
        MenuItem itemShutter = mActionMenu.getMenu().findItem(R.id.action_shutter);

        int id = mCamera != null ? mCamera.getId() : 0;
        switch (id) {
            default:
            case MK58Camera.ID:
            case CGO2Camera.ID:
                enableMenuItem(itemExposure, false);
                enableMenuItem(itemISO, false);
                enableMenuItem(itemShutter, false);
                enableMenuItem(itemWB, false);
                break;
//            case CGO3Camera.ID:
//                enableMenuItem(itemExposure, true);
//                enableMenuItem(itemISO, true);
//                enableMenuItem(itemShutter, true);
//                enableMenuItem(itemWB, true);
//                break;
        }
    }

    private void enableMenuItem(MenuItem item, boolean enable) {
        item.setVisible(enable);
        item.setEnabled(enable);
    }

    private void updateUI() {
        mShutterReleaseButton.setVisibility(
                mCamera != null &&
                        mCamera.isConnected() &&
                        (mCamera.supportsCommand(Command.SNAPSHOT) ||
                                mCamera.supportsCommand(Command.START_RECORDING)) ?
                        View.VISIBLE :
                        View.INVISIBLE);

        mShutterReleaseButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mCamera.updateStatus(mHandler);
            }
        });

    }

}
