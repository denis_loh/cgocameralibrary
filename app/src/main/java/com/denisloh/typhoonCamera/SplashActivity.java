package com.denisloh.typhoonCamera;

import android.Manifest;
import android.content.Intent;
import android.content.IntentSender;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.os.Message;
import android.preference.PreferenceManager;
import android.provider.Settings;
import android.support.annotation.NonNull;
import android.support.design.widget.Snackbar;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.TextView;

import com.crashlytics.android.Crashlytics;
import com.crashlytics.android.core.CrashlyticsCore;
import com.denisloh.cgocamera.CGOCamera;
import com.denisloh.cgocamera.CGOCameraManager;
import com.denisloh.cgocamera.utils.WeakHandler;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.PendingResult;
import com.google.android.gms.common.api.ResultCallback;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.location.LocationSettingsRequest;
import com.google.android.gms.location.LocationSettingsResult;
import com.google.android.gms.location.LocationSettingsStatusCodes;

import java.util.List;

import io.fabric.sdk.android.Fabric;

public class SplashActivity extends AppCompatActivity implements CGOCameraManager.OnCameraStateChangedListener {

    private static final String TAG = SplashActivity.class.getSimpleName();

    private static final int REQUEST_LOCATION = 1;
    private static final int REQUEST_CHECK_SETTINGS = 2;

    private CGOCameraManager mCGOCameraManager;
    private TextView mStatusTV;
    private boolean isLocationServiceAvailable;
    private InitHandler mHandler;

    private static class InitHandler extends WeakHandler<SplashActivity> {
        InitHandler(SplashActivity owner) {
            super(owner, owner.getMainLooper());
        }

        @Override
        public void handleMessage(Message msg) {
            if (CGOCamera.Command.INIT_CAMERA.getCommandId() == msg.what) {
                boolean result = msg.arg1 == CGOCamera.NO_ERROR;

                if (!result) {
                    Log.d(TAG, "Error: (" + msg.arg1 + ")" + msg.getData().getSerializable(CGOCamera.KEY_EXCEPTION));
                }

                getOwner().onInitialized(result);
            }
        }
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Fabric.with(this, new Crashlytics.Builder().core(new CrashlyticsCore.Builder().disabled(BuildConfig.DEBUG).build()).build());

        setContentView(R.layout.activity_splash);

        mCGOCameraManager = CGOCameraManager.getInstance(this);
        mHandler = new InitHandler(this);
        mStatusTV = (TextView) findViewById(R.id.status);

        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION)
                != PackageManager.PERMISSION_GRANTED) {
            // Check Permissions Now
            ActivityCompat.requestPermissions(this,
                    new String[]{Manifest.permission.ACCESS_COARSE_LOCATION},
                    REQUEST_LOCATION);
        } else {
            // permission has been granted, continue as usual
            enableLocationService();
        }
    }

    @Override
    protected void onResume() {
        super.onResume();

        mCGOCameraManager.registerOnCameraConnectedListener(this);

        CGOCamera camera = CGOCameraManager.getCamera();
        if (camera != null && camera.isConnected()) {
            onInitialized(true);
        }
    }

    @Override
    protected void onPause() {
        super.onPause();

        mCGOCameraManager.unregisterOnCameraConnectedListener(this);
    }

    public void onRequestPermissionsResult(int requestCode,
                                           @NonNull String[] permissions,
                                           @NonNull int[] grantResults) {
        if (requestCode == REQUEST_LOCATION) {
            if(grantResults.length == 1 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                // We can now safely use the API we requested access to
                enableLocationService();
            } else {
                Log.d(TAG, "Permission for wifi scan denied.");
                onLocationServiceUnavailable();
            }
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        switch (requestCode) {
            case REQUEST_CHECK_SETTINGS:
                switch (resultCode) {
                    case RESULT_OK:
                        // All required changes were successfully made
                        onLocationServiceAvailable();
                        break;
                    case RESULT_CANCELED:
                        // The user was asked to change settings, but chose not to
                        onLocationServiceUnavailable();
                        break;
                    default:
                        break;
                }
                break;
        }

        super.onActivityResult(requestCode, resultCode, data);
    }

    //region Location service
    private void enableLocationService() {
        GoogleApiClient googleClient = new GoogleApiClient.Builder(this)
                .addApi(LocationServices.API)
                .build();

        googleClient.connect();

        LocationRequest locationRequest = new LocationRequest();

        // We scan with the lowest priority, so that we have only minimal battery impact.
        locationRequest.setPriority(LocationRequest.PRIORITY_LOW_POWER);

        LocationSettingsRequest.Builder builder = new LocationSettingsRequest.Builder()
                .setAlwaysShow(true)
                .addLocationRequest(locationRequest);

        builder.build();

        PendingResult<LocationSettingsResult> result =
                LocationServices.SettingsApi.checkLocationSettings(googleClient, builder.build());

        result.setResultCallback(new ResultCallback<LocationSettingsResult>() {
            @Override
            public void onResult(@NonNull LocationSettingsResult locationSettingsResult) {
                final Status status = locationSettingsResult.getStatus();
                switch (status.getStatusCode()) {
                    case LocationSettingsStatusCodes.SUCCESS:
                        // All location settings are satisfied. The client can initialize location
                        // requests here.
                        onLocationServiceAvailable();
                        break;
                    case LocationSettingsStatusCodes.RESOLUTION_REQUIRED:
                        // Location settings are not satisfied. But could be fixed by showing the user
                        // a dialog.
                        try {
                            // Show the dialog by calling startResolutionForResult(),
                            // and check the result in onActivityResult().
                            status.startResolutionForResult(
                                    SplashActivity.this,
                                    REQUEST_CHECK_SETTINGS);
                        } catch (IntentSender.SendIntentException e) {
                            // Ignore the error.
                        }
                        break;
                    case LocationSettingsStatusCodes.SETTINGS_CHANGE_UNAVAILABLE:
                        // Location settings are not satisfied. However, we have no way to fix the
                        // settings so we won't show the dialog.
                        onLocationServiceUnavailable();
                        break;
                }
            }
        });
    }

    private void onLocationServiceAvailable() {
        Log.d(TAG, "Location services are enabled...");
        isLocationServiceAvailable = true;
        connectToCamera();
    }

    private void onLocationServiceUnavailable() {
        Log.w(TAG, "Location services are not available...");
        isLocationServiceAvailable = false;
        connectToCamera();
    }
    //endregion

    private void showWifiAvailableSnackBar(final List<String> results) {
        Snackbar.make(findViewById(android.R.id.content), R.string.wifi_available, Snackbar.LENGTH_INDEFINITE)
                .setAction(R.string.connect, new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        mCGOCameraManager.connect(results.get(0));
                    }
                })
                .show();
    }

    private void showWifiSettingsSnackBar(final String message) {
        Snackbar.make(
                findViewById(android.R.id.content),
                getString(R.string.wifi_error_open_settings, message),
                Snackbar.LENGTH_INDEFINITE)
                .setAction(R.string.wifi_settings, new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        startActivity(new Intent(Settings.ACTION_WIFI_SETTINGS));
                    }
                })
                .show();
        Log.d(TAG, "Showing snackbar.");
    }

    private void connectToCamera() {
        if (!mCGOCameraManager.isWifiEnabled()) {
            mStatusTV.setText(R.string.status_offline);
            showWifiSettingsSnackBar(getString(R.string.error_wifi_deactivated));
            return;
        }

        mStatusTV.setText(R.string.status_connecting);

        SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(this);

        // Auto connect is default here.
        int auto_connect;
        try {
            auto_connect = Integer.parseInt(prefs.getString("wifi_auto_connect", "2"));
        } catch (NumberFormatException nfe) {
            auto_connect = 2;
        }

        if (isLocationServiceAvailable && auto_connect > 0) {
            Log.d(TAG, "Checking for camera wifis");

            List<String> results = mCGOCameraManager.getCameraSSIDs();
            Log.d(TAG, "Found " + results.size() + " cameras");

            if (results.size() > 0) {
                if (auto_connect > 1) {
                    mCGOCameraManager.connect(results.get(0));
                } else {
                    showWifiAvailableSnackBar(results);
                }
            } else {
                showWifiSettingsSnackBar(getString(R.string.error_no_camera_wifis_found));
            }
        } else {
            mCGOCameraManager.connect();
        }

    }

    @Override
    public void onCamerasDiscovered(List<String> SSID) {
        // Ignore
    }

    @Override
    public void onConnected(CGOCamera camera) {
        mStatusTV.setText(R.string.status_initializing);

        camera.initCamera(mHandler);
    }

    @Override
    public void onConnectionFailed(String SSID) {
        Log.e(TAG, "Connection to SSID " + SSID + " failed.");
        mStatusTV.setText(getString(R.string.error_wifi_connection_failed, SSID));
    }

    @Override
    public void onDisconnected() {
        // Ignore
    }

    private void onInitialized(boolean success) {
        if (success) {
            Log.d(TAG, "Ready to fly...");
            mStatusTV.setText(R.string.status_ready);
            startActivity(new Intent(this, MainActivity.class));
            finish();
        } else {
            mStatusTV.setText(R.string.error_initialization_failed);
        }
    }
}
