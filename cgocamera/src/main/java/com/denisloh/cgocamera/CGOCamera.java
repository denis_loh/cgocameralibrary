package com.denisloh.cgocamera;

import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.os.Parcelable;
import android.util.Log;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.denisloh.cgocamera.utils.CommandException;
import com.denisloh.cgocamera.utils.ParseException;
import com.denisloh.cgocamera.utils.ResponseException;

import java.io.Serializable;
import java.net.ConnectException;
import java.net.InetSocketAddress;
import java.net.Socket;
import java.net.SocketAddress;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@SuppressWarnings("SameParameterValue")
public abstract class CGOCamera {

    private static final String TAG = CGOCamera.class.getSimpleName();
    
    private final CameraParameters mCurrentStatus = new CameraParameters();
    private CGOCameraManager mCameraManager;
    private String mWifiSSID;

    public static final int NO_ERROR = 0;
    public static final int ERROR_COMMAND_FAILED = -1;
    public static final int ERROR_PARSE_FAILED = -2;
    public static final int ERROR_NETWORK_FAILED = -3;

    public static final int ERROR_COMMAND_NOT_SUPPORTED = -102;
    public static final int ERROR_CAMERA_NOT_INITIALIZED = -105;

    public static final String KEY_DATA = "com.denisloh.cgocamera.data.Data";
    public static final String KEY_EXCEPTION = "com.denisloh.cgocamera.data.Exception";

    protected class Parameter {
        public final String Key;
        public final String Value;

        public Parameter(String key, String value) {
            Key = key;
            Value = value;
        }
    }

    //region Enumerations

    public enum Command {
        PING,

        INIT_CAMERA,
        GET_STATUS,
        GET_FIRMWARE_INFO,
        RESET_DEFAULT,
        SET_TIME,

        SET_CAMERA_MODE,
        GET_CAMERA_MODE,

        START_RECORDING,
        STOP_RECORDING,
        SET_AUDIO_SW,
        GET_AUDIO_SW,
        SET_VIDEO_MODE,
        GET_VIDEO_MODE,
        GET_RECORD_TIME,

        SNAPSHOT,
        SET_EXPOSURE_VALUE,
        GET_EXPOSURE_VALUE,
        SET_WHITE_BALANCE_MODE,
        GET_WHITE_BALANCE_MODE,
        SET_IMAGE_QUALITY_TYPE,
        GET_IMAGE_QUALITY_TYPE,
        SET_PHOTO_FORMAT,
        GET_PHOTO_FORMAT,
        SET_SHUTTER_TIME,
        GET_SHUTTER_TIME,
        SET_ISO_SENSITIVITY,
        GET_ISO_SENSITIVITY,
        SET_SHARPNESS,
        GET_SHARPNESS,
        SET_AUTO_EXPOSURE_ENABLE,
        GET_AUTO_EXPOSURE_ENABLE,

        FORMAT_SDCARD,
        GET_SDCARD_TOTAL_SPACE,
        GET_SDCARD_FREE_SPACE,

        USER_COMMAND;

        private int CommandId;

        Command() {
            CommandId = this.ordinal();
        }

        public int getCommandId() {
            return CommandId;
        }

        public void setCommandId(int commandId) throws IllegalArgumentException {
            if (this == USER_COMMAND && commandId >= Command.values().length) {
                CommandId = commandId;
            } else {
                throw new IllegalArgumentException("Not a user command. Id must be greater than "
                        + Command.values().length);
            }
        }
    }

    public enum StatusMode {
        VIEWFINDER("vf"),
        RECORDING("record");

        public final String ModeString;

        StatusMode(String status) {
            ModeString = status;
        }

        public static StatusMode fromString(String status) {
            if (VIEWFINDER.ModeString.equals(status)) {
                return VIEWFINDER;
            } else if (RECORDING.ModeString.equals(status)) {
                return RECORDING;
            } else {
                return null;
            }
        }
    }

    public enum PhotoFormat {
        JPG("jpg"),
        DNG("dng");

        public final String FormatString;

        PhotoFormat(String format) {
            FormatString = format;
        }

        public static PhotoFormat fromString(String format) {
            if (JPG.FormatString.equals(format)) {
                return JPG;
            } else if (DNG.FormatString.equals(format)) {
                return DNG;
            } else {
                return null;
            }
        }
    }

    public enum CameraMode {
        VIDEO("video", 1),
        PHOTO("photo", 2);

        public final String ModeString;
        public final int ModeId;

        CameraMode(String mode, int id) {
            ModeString = mode;
            ModeId = id;
        }

        public static CameraMode fromInt(int i) {
            switch (i) {
                case 1:
                    return VIDEO;
                case 2:
                    return PHOTO;
                default:
                    return null;
            }
        }
    }

    public enum WhiteBalanceMode {
        AUTO(0),
        INCANDESCENT(1),
        SUNSET(3),
        SUNNY(4),
        CLOUDY(5),
        FLUORESCENT(7),
        LOCK(99);

        public final int Id;

        WhiteBalanceMode(int mode) {
            Id = mode;
        }

        public static WhiteBalanceMode fromInt(int mode) {
            switch (mode) {
                case 0:
                    return AUTO;
                case 1:
                    return INCANDESCENT;
                case 3:
                    return SUNSET;
                case 4:
                    return SUNNY;
                case 5:
                    return CLOUDY;
                case 7:
                    return FLUORESCENT;
                case 99:
                    return LOCK;
                default:
                    return null;
            }
        }
    }

    public enum ImageQualityMode {
        NATURAL(0),
        GORGEOUS(1),
        RAW(2),
        NIGHT(3);

        public final int Id;

        ImageQualityMode(int mode) {
            Id = mode;
        }

        public static ImageQualityMode fromInt(int mode) {
            switch (mode) {
                case 0:
                    return NATURAL;
                case 1:
                    return GORGEOUS;
                case 2:
                    return RAW;
                case 3:
                    return NIGHT;
                default:
                    return null;
            }
        }
    }

    public static class VideoMode {
        public final String Resolution;
        public final String FrameRate;

        public VideoMode(String resolution, String frameRate) {
            this.Resolution = resolution;
            this.FrameRate = frameRate;
        }

        @Override
        public String toString() {
            return "VideoMode{" +
                    "Resolution='" + Resolution + '\'' +
                    ", FrameRate='" + FrameRate + '\'' +
                    '}';
        }
    }
    //endregion

    //region Camera identifiers

    public abstract int getId();

    public abstract String getName();

    public abstract String getLiveStreamUrl();

    public abstract boolean supportsCommand(Command command);

    //endregion

    //region Camera command functions
    // These functions can be used to communicate with the camera.

    public void ping(Handler handler) {
        performPingRequest(handler, getLiveStreamUrl(), Command.PING);
    }

    public void initCamera(Handler handler) {
        performEmptyRequest(handler, Command.INIT_CAMERA);
    }

    public void updateStatus(Handler handler) {
        performEmptyRequest(handler, Command.GET_STATUS);
    }

    public void getFirmwareInfo(Handler handler) {
        performEmptyRequest(handler, Command.GET_FIRMWARE_INFO);
    }

    public void resetDefaults(Handler handler) {
        performEmptyRequest(handler, Command.RESET_DEFAULT);
    }

    public void setTime(Handler handler, Date time) {
        performEmptyRequest(handler, Command.SET_TIME);
    }

    public void setCameraMode(Handler handler, CameraMode mode) {
        performEmptyRequest(handler, Command.SET_CAMERA_MODE);
    }

    public void getCameraMode(Handler handler) {
        performEmptyRequest(handler, Command.GET_CAMERA_MODE);
    }

    public void startRecord(Handler handler) {
        performEmptyRequest(handler, Command.START_RECORDING);
    }

    public void stopRecord(Handler handler) {
        performEmptyRequest(handler, Command.STOP_RECORDING);
    }

    public void setAudioSwitch(Handler handler, boolean audioOn) {
        performEmptyRequest(handler, Command.SET_AUDIO_SW);
    }

    public void getAudioSwitch(Handler handler) {
        performEmptyRequest(handler, Command.GET_AUDIO_SW);
    }

    public void setVideoMode(Handler handler, String mode) {
        performEmptyRequest(handler, Command.SET_VIDEO_MODE);
    }

    public void getVideoMode(Handler handler) {
        performEmptyRequest(handler, Command.GET_VIDEO_MODE);
    }

    public List<VideoMode> getVideoModes() {
        return new ArrayList<>();
    }

    public void getRecordTime(Handler handler) {
        performEmptyRequest(handler, Command.GET_RECORD_TIME);
    }

    public void snapshot(Handler handler) {
        performEmptyRequest(handler, Command.SNAPSHOT);
    }

    public void setExposureValue(Handler handler, double exposureValue) {
        performEmptyRequest(handler, Command.SNAPSHOT);
    }

    public void getExposureValue(Handler handler) {
        performEmptyRequest(handler, Command.SNAPSHOT);
    }

    public void setWhiteBalanceMode(Handler handler, WhiteBalanceMode mode) {
        performEmptyRequest(handler, Command.SET_WHITE_BALANCE_MODE);
    }

    public void getWhiteBalanceMode(Handler handler) {
        performEmptyRequest(handler, Command.GET_WHITE_BALANCE_MODE);
    }
    
    public void setImageQualityMode(Handler handler, ImageQualityMode mode) {
        performEmptyRequest(handler, Command.SET_IMAGE_QUALITY_TYPE);
    }
    
    public void getImageQualityMode(Handler handler) {
        performEmptyRequest(handler, Command.GET_IMAGE_QUALITY_TYPE);
    }
    
    public void setShutterTime(Handler handler, int shutterTime) {
        performEmptyRequest(handler, Command.SET_SHUTTER_TIME);
    }
    
    public void getShutterTime(Handler handler) {
        performEmptyRequest(handler, Command.GET_SHUTTER_TIME);
    }

    public void setISOSensitivity(Handler handler, String iso) {
        performEmptyRequest(handler, Command.SET_ISO_SENSITIVITY);
    }

    public void getISOSensitivity(Handler handler) {
        performEmptyRequest(handler, Command.GET_ISO_SENSITIVITY);
    }

    public void setSharpness(Handler handler, int sharpness) {
        performEmptyRequest(handler, Command.SET_SHARPNESS);
    }

    public void getSharpness(Handler handler) {
        performEmptyRequest(handler, Command.GET_SHARPNESS);
    }

    public void setAutoExposure(Handler handler, boolean aeEnabled) {
        performEmptyRequest(handler, Command.SET_AUTO_EXPOSURE_ENABLE);
    }
    
    public void getAutoExposure(Handler handler) {
        performEmptyRequest(handler, Command.GET_AUTO_EXPOSURE_ENABLE);
    }
    
    public void setPhotoFormat(Handler handler, PhotoFormat format) {
        performEmptyRequest(handler, Command.SET_PHOTO_FORMAT);
    }

    public void getPhotoFormat(Handler handler) {
        performEmptyRequest(handler, Command.GET_PHOTO_FORMAT);
    }
    
    public void formatSDCard(Handler handler) {
        performEmptyRequest(handler, Command.FORMAT_SDCARD);
    }
    
    public void getSDCardTotalSpace(Handler handler) {
        performEmptyRequest(handler, Command.GET_SDCARD_TOTAL_SPACE);
    }
    
    public void getSDCardFreeSpace(Handler handler) {
        performEmptyRequest(handler, Command.GET_SDCARD_FREE_SPACE);
    }

    //endregion

    public CameraParameters getCurrentStatus() {
        return mCurrentStatus;
    }

    //region Protected or private functions

    protected Object parseResponse(Command command, String response) throws ParseException, CommandException {
        Log.e(TAG, "Unsupported command: " + command + " with response: " + response);

        return null;
    }

    protected void performEmptyRequest(final Handler handler, final Command command) {
        if (handler != null) {
            Message message = prepareMessage(handler, command, NO_ERROR, null);
            message.sendToTarget();
        }
    }

    private void performPingRequest(final Handler handler, final String baseUri, final Command command) {
        new Thread(new Runnable() {
            @Override
            public void run() {
                Uri uri = Uri.parse(baseUri);
                int state = NO_ERROR;
                try {
                    SocketAddress sockaddr = new InetSocketAddress(
                            uri.getHost(),
                            uri.getPort() == -1 ? 80 : uri.getPort());
                    // Create an unbound socket
                    Socket sock = new Socket();

                    // This method will block no more than timeoutMs.
                    // If the timeout occurs, SocketTimeoutException is thrown.
                    int timeoutMs = 2000;   // 2 seconds
                    sock.connect(sockaddr, timeoutMs);
                }catch (ConnectException ce){
                    state = ERROR_NETWORK_FAILED;
                }catch (Exception e) {
                    state = ERROR_COMMAND_FAILED;
                }

                if (handler != null) {
                    Message message = prepareMessage(handler, command, state, null);
                    message.sendToTarget();
                }
            }
        }).start();
    }

    protected void performLocalRequest(final Handler handler, final Command command, int state, Object data) {
        Message message = prepareMessage(handler, command, state, data);
        message.sendToTarget();
    }

    protected void performRequest(final Handler handler, final String baseUri, final Command command, List<Parameter> params) {
        Uri uri = Uri.parse(baseUri);
        Uri.Builder builder = uri.buildUpon();

        for (Parameter param : params) {
            builder.appendQueryParameter(param.Key, param.Value);
        }

        final StringRequest request = new StringRequest(Request.Method.GET, builder.toString(), new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                if (handler != null) {
                    Object data;
                    int state;

                    try {
                        data = parseResponse(command, response);
                        state = NO_ERROR;
                    } catch (ResponseException e) {
                        data = e;
                        state = e.ResponseCode;
                    } catch (CommandException e) {
                        data = e;
                        state = ERROR_COMMAND_FAILED;
                    } catch (Exception e) {
                        data = e;
                        state = ERROR_PARSE_FAILED;
                    }

                    Message message = prepareMessage(handler, command, state, data);
                    message.sendToTarget();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Message message = prepareMessage(handler, command, ERROR_NETWORK_FAILED, error);
                message.sendToTarget();
            }
        });

        request.setRetryPolicy(new DefaultRetryPolicy());

        mCameraManager.getRequestQueue().add(request);
    }

    private Message prepareMessage(Handler handler, Command command, int responseCode, Object data) {
        Message message = handler.obtainMessage(command.ordinal());
        message.arg1 = responseCode;

        if (data != null) {
            Bundle bundle = new Bundle();
            if (responseCode == NO_ERROR) {
                if (data instanceof Serializable) {
                    bundle.putSerializable(KEY_DATA, (Serializable) data);
                } else if (data instanceof Parcelable) {
                    bundle.putParcelable(KEY_DATA, (Parcelable) data);
                } else {
                    Log.w(TAG, "Data is neither Serializable nor Parcelable.");
                }
            } else {
                bundle.putSerializable(KEY_EXCEPTION, (Throwable) data);
            }
            message.setData(bundle);
        }

        return message;
    }
    //endregion

    public String getWifiSSID() {
        return this.mWifiSSID;
    }

    public boolean isConnected() {
        return this.mCameraManager != null && this.mCameraManager.isConnected(getWifiSSID());
    }

    public void connect() {
        Log.d(TAG, "Connecting " + getName() + " to " + getWifiSSID());
        if (this.mCameraManager != null) {
            this.mCameraManager.connect(getWifiSSID());
        }
    }

    void setWifiSSID(String wifiSSID) {
        this.mWifiSSID = wifiSSID;
    }

    void setCameraManager(CGOCameraManager mCameraManager) {
        this.mCameraManager = mCameraManager;
    }
}
