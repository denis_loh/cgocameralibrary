package com.denisloh.cgocamera;

import android.annotation.SuppressLint;
import android.content.Context;
import android.os.AsyncTask;
import android.os.Message;
import android.support.annotation.NonNull;
import android.util.Log;

import com.android.volley.RequestQueue;
import com.android.volley.toolbox.Volley;
import com.denisloh.cgocamera.cameras.CGO2Camera;
import com.denisloh.cgocamera.cameras.CGO3Camera;
import com.denisloh.cgocamera.cameras.MK58Camera;
import com.denisloh.cgocamera.utils.WeakHandler;

import java.util.ArrayList;
import java.util.List;

/**
 * CGO Camera Manager
 *
 * This is the main manager class for communicating with a CGO camera.
 *
 * Use this class as an entry point for creating your own apps.
 */
@SuppressWarnings("SameParameterValue")
public class CGOCameraManager {

    public static final String PREFIX_MK58 = "MK58";
    public static final String PREFIX_LK58 = "LK58";
    public static final String PREFIX_CGO = "CGO";

    private static final int MAX_PING_RETRIES = 3;
    private static final String TAG = CGOCameraManager.class.getSimpleName();

    // Using application context is considered not to be a memory leak.
    @SuppressLint("StaticFieldLeak")
    private static final CGOCameraManager mInstance = new CGOCameraManager();
    private static CGOCamera sCurrentCamera;
    private static RequestQueue sRequestQueue;

    private static final String[] CGO_WIFI_PREFIXES = {
            PREFIX_CGO,
            PREFIX_LK58,
            PREFIX_MK58
    };

    private CGOCamera mNewCamera;
    private int mSelectedCameraModel;
    private PingHandler mPingHandler;
    private Context mContext;
    private WifiHelper mWifiHelper;
    private boolean mIsInitialized = false;
    private final List<OnCameraStateChangedListener> mListeners = new ArrayList<>();

    private static class PingHandler extends WeakHandler<CGOCameraManager> {
        private int pingRetries = 0;

        PingHandler(CGOCameraManager owner) {
            super(owner, owner.mContext.getMainLooper());
        }

        @Override
        public void handleMessage(Message msg) {
            final CGOCamera camera = getOwner().mNewCamera;

            if (CGOCamera.Command.PING.getCommandId() == msg.what) {
                if (msg.arg1 == CGOCamera.NO_ERROR) {
                    Log.d(TAG, "Ping succeeded...");
                    sCurrentCamera = camera;
                    for (OnCameraStateChangedListener listener : getOwner().mListeners) {
                        listener.onConnected(sCurrentCamera);
                        pingRetries = 0;
                    }
                } else if (msg.arg1 == CGOCamera.ERROR_NETWORK_FAILED && pingRetries < MAX_PING_RETRIES) {
                    Log.d(TAG, "Ping failed... retry.");

                    new AsyncTask<Void, Void, Void>() {

                        @Override
                        protected Void doInBackground(Void... params) {
                            try {
                                // Sleep for two seconds until we retry.
                                Thread.sleep(2000);
                                camera.ping(PingHandler.this);
                                ++pingRetries;
                            } catch (InterruptedException e) {
                                for (OnCameraStateChangedListener listener : getOwner().mListeners) {
                                    listener.onConnectionFailed(camera.getWifiSSID());
                                    pingRetries = 0;
                                }
                            }
                            return null;
                        }
                    }.execute();
                } else {
                    Log.d(TAG, "Ping failed...");
                    for (OnCameraStateChangedListener listener : getOwner().mListeners) {
                        listener.onConnectionFailed(camera.getWifiSSID());
                        pingRetries = 0;
                    }
                }
            }
        }
    }

    private final WifiHelper.OnWifiStateChangedListener mOnWifiStateChangedListener = new WifiHelper.OnWifiStateChangedListener() {

        @Override
        public void onWifiConnected(final String SSID) {
            Log.d(TAG, "Connected to wifi with SSID " + SSID);

            initializeCamera(SSID);
        }

        @Override
        public void onWifiDisconnected() {
            Log.d(TAG, "Disconnected from wifi");
            for (OnCameraStateChangedListener listener : mListeners) {
                listener.onDisconnected();
            }
        }

        @Override
        public void onWifiDiscovered(List<String> SSIDs) {
            Log.d(TAG, "Discovered " + SSIDs.size() + " wifis nearby");
            if (sCurrentCamera == null || !mWifiHelper.isConnectedToWifi(sCurrentCamera.getWifiSSID())) {
                List<String> wifis = getCameraSSIDs();
                if (wifis.size() > 0) {
                    for (OnCameraStateChangedListener listener : mListeners) {
                        listener.onCamerasDiscovered(SSIDs);
                    }
                }
            }
        }

        @Override
        public void onWifiConnectionFailed(String SSID) {
            Log.d(TAG, "Wifi connection failed to SSID " + SSID);
            for (OnCameraStateChangedListener listener : mListeners) {
                listener.onConnectionFailed(SSID);
            }
        }
    };

    public interface OnCameraStateChangedListener {
        void onCamerasDiscovered(List<String> SSID);
        void onConnected(CGOCamera camera);
        void onConnectionFailed(String SSID);
        void onDisconnected();
    }

    public static CGOCameraManager getInstance(@NonNull Context context) {
        mInstance.init(context);
        return mInstance;
    }

    public static CGOCamera getCamera() {
        if (!mInstance.mIsInitialized) {
            Log.e(TAG, "Not yet initialized");
            return null;
        }
        return sCurrentCamera;
    }

    public ArrayList<String> getCameraSSIDs() {
        ArrayList<String> cameraWifi = new ArrayList<>();

        List<String> wifiSSIDs = mWifiHelper.getWifiSSIDs();
        for (String SSID : wifiSSIDs) {
            if (isCameraSSID(SSID)) {
                cameraWifi.add(SSID);
            }
        }

        return cameraWifi;
    }

    private boolean isCameraSSID(String ssid) {
        for (String prefix : CGO_WIFI_PREFIXES) {
            if (ssid.startsWith(prefix)) {
                return true;
            }
        }
        return false;
    }

    boolean isConnected(String cameraSSID) {
        return mWifiHelper.isConnectedToWifi(cameraSSID);
    }

    public void connect() {
        connect(mWifiHelper.getCurrentWifiSSID());
    }

    public void connect(@NonNull String SSID) {
        connect(SSID, 0);
    }

    public void connect(@NonNull String SSID, int cameraId) {
        mSelectedCameraModel = cameraId;
        connectInternal(SSID);
    }

    public void disconnect() {
        // Unregister receiver
        mWifiHelper.stop();

        Log.d(TAG, "uninitialized camera manager");
    }

    public boolean isWifiEnabled() {
        return mWifiHelper.isWifiEnabled();
    }

    public void registerOnCameraConnectedListener(OnCameraStateChangedListener listener) {
        mListeners.add(listener);
    }

    public void unregisterOnCameraConnectedListener(OnCameraStateChangedListener listener) {
        mListeners.remove(listener);
    }

    //region Private functions

    private CGOCameraManager() {
    }

    private void init(@NonNull Context context) {
        if (!mIsInitialized) {
            mContext = context.getApplicationContext();

            sRequestQueue = Volley.newRequestQueue(mContext);
            mPingHandler = new PingHandler(this);

            mWifiHelper = new WifiHelper(mContext, mOnWifiStateChangedListener);
            mWifiHelper.start();
            mIsInitialized = true;

            Log.d(TAG, "initialized camera manager");
        }
    }

    private void initializeCamera(String SSID) {
        int model = mSelectedCameraModel != 0 ? mSelectedCameraModel : detectCameraNameBySSID(SSID);

        mNewCamera = createCameraById(model);

        if (mNewCamera != null) {

            mNewCamera.setCameraManager(this);
            mNewCamera.setWifiSSID(SSID);

            Log.d(TAG, "ping camera...");
            mNewCamera.ping(mPingHandler);
        } else {
            for (OnCameraStateChangedListener listener : mListeners) {
                listener.onConnectionFailed(SSID);
            }
        }
    }

    private int detectCameraNameBySSID(String SSID) {
        if (SSID != null) {
            if (SSID.startsWith(PREFIX_MK58)) {
                return MK58Camera.ID;
            } else if (SSID.startsWith(PREFIX_CGO)) {
                try {
                    char version = SSID.charAt(PREFIX_CGO.length());
                    switch (version) {
                        case '2':
                            return CGO2Camera.ID;
                        case '3':
                            return CGO3Camera.ID;
                        default:
                            break;
                    }
                } catch (IndexOutOfBoundsException ioobe) {
                    // The SSID is not a valid CGO camera wifi or was renamed by the user.
                }
            }
        }

        return 0;
    }

    private CGOCamera createCameraById(int id) {
        switch (id) {
            case CGO2Camera.ID:
                return new CGO2Camera();
            case CGO3Camera.ID:
                return new CGO3Camera();
            case MK58Camera.ID:
                return new MK58Camera();
            default:
                return null;
        }
    }

    private void connectInternal(String SSID) {
        Log.i(TAG, "Connecting to camera with SSID \"" + SSID + "\"");
        mWifiHelper.connectToWifi(SSID);
    }

    //endregion

    //region Package private functions

    RequestQueue getRequestQueue() {
        return sRequestQueue;
    }

    //endregion Package private functions

}
