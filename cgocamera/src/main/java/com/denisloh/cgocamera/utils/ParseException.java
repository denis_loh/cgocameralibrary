package com.denisloh.cgocamera.utils;

/**
 * Parse exception
 *
 * Thrown whenever an error occurred while parsing the response
 * of the camera
 */
public class ParseException extends Exception {

    /**
     * Construct a parse exception
     * @param detailMessage A detailed message of the reason of failure.
     */
    public ParseException(String detailMessage) {
        super(detailMessage);
    }

    /**
     * Construct a parse exception
     * @param detailMessage A detailed message of the reason of failure.
     * @param throwable The throwable object which caused the failure
     */
    public ParseException(String detailMessage, Throwable throwable) {
        super(detailMessage, throwable);
    }

    /**
     * Construct a parse exception
     * @param throwable The throwable object which caused the failure
     */
    public ParseException(Throwable throwable) {
        super(throwable);
    }
}
