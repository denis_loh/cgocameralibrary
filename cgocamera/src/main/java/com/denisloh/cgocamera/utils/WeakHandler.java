package com.denisloh.cgocamera.utils;

import android.os.Handler;
import android.os.Looper;

import java.lang.ref.WeakReference;

/**
 * Weak handler with reference to its owner
 *
 * This specialized handler includes a weak reference to its owner.
 * The reference is released whenever this object should be freed,
 * so it avoids memory leaks.
 *
 */
public class WeakHandler<T> extends Handler {
    private final WeakReference<T> owner;

    /**
     * Creates a new weak handler of type <T>
     *
     * @param owner The owner object which should be referenced
     */
    public WeakHandler(T owner) {
        this.owner = new WeakReference<>(owner);
    }

    public WeakHandler(T owner, Looper looper) {
        super(looper);

        this.owner = new WeakReference<>(owner);
    }

    /**
     * Returns the owner object
     *
     * @return the owner or <i>null</i> if not available.
     */
    public T getOwner() {
        return owner.get();
    }
}
