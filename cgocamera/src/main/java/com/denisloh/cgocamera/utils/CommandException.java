package com.denisloh.cgocamera.utils;

import com.denisloh.cgocamera.CGOCamera;

/**
 * Command exception
 *
 * Thrown whenever the execution of a command failed or returned
 * an error.
 */
public class CommandException extends Exception {

    public final CGOCamera.Command FailedCommand;

    /**
     * Construct a command exception
     * @param command The command which failed
     * @param detailMessage A detailed message of the reason of failure.
     */
    public CommandException(CGOCamera.Command command, String detailMessage) {
        super(detailMessage);
        FailedCommand = command;
    }

    /**
     * Construct a command exception
     * @param command The command which failed
     * @param detailMessage A detailed message of the reason of failure
     * @param throwable The throwable object which caused the failure
     */
    public CommandException(CGOCamera.Command command, String detailMessage, Throwable throwable) {
        super(detailMessage, throwable);

        FailedCommand = command;
    }
}
