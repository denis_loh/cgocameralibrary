package com.denisloh.cgocamera.utils;

import com.denisloh.cgocamera.CGOCamera;

public class ResponseException extends CommandException {

    public final int ResponseCode;

    public ResponseException(CGOCamera.Command command, int responseCode, String detailMessage) {
        super(command, detailMessage);

        ResponseCode = responseCode;
    }

    public ResponseException(CGOCamera.Command command, int responseCode, String detailMessage, Throwable throwable) {
        super(command, detailMessage, throwable);

        ResponseCode = responseCode;
    }
}
