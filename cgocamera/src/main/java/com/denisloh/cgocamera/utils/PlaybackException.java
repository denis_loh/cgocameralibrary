package com.denisloh.cgocamera.utils;

public class PlaybackException extends Exception {

    public PlaybackException(String detailMessage) {
        super(detailMessage);
    }

    public PlaybackException(String detailMessage, Throwable throwable) {
        super(detailMessage, throwable);
    }

}
