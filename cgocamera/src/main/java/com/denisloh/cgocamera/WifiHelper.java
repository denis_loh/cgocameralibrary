package com.denisloh.cgocamera;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.wifi.ScanResult;
import android.net.wifi.SupplicantState;
import android.net.wifi.WifiConfiguration;
import android.net.wifi.WifiInfo;
import android.net.wifi.WifiManager;
import android.os.Build;
import android.os.HandlerThread;
import android.os.Looper;
import android.os.Message;
import android.util.Log;

import com.denisloh.cgocamera.utils.ReflectionHelper;
import com.denisloh.cgocamera.utils.WeakHandler;

import java.lang.reflect.InvocationTargetException;
import java.net.InetAddress;
import java.util.ArrayList;
import java.util.List;

class WifiHelper {

    interface OnWifiStateChangedListener {
        void onWifiConnected(String SSID);
        void onWifiConnectionFailed(String SSID);
        void onWifiDisconnected();
        void onWifiDiscovered(List<String> wifiSSIDs);
    }

    private static final int WIFI_CONNECT = 1;

    private static final int STATE_CONNECTED = 1;
    private static final int STATE_CONNECTION_FAILED = 2;
    private static final int STATE_DISCONNECTED = 3;
    private static final int STATE_WIFI_DISCOVERED = 4;

    private static final String TAG = WifiHelper.class.getSimpleName();

    private final Context mContext;
    private final WifiHandler mHandler;
    private final StateHandler mStateHandler;
    private final OnWifiStateChangedListener mWifiStateChangedListener;
    private boolean mIsReceiverRegistered = false;

    private final BroadcastReceiver mWifiConnectedReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            if (mWifiStateChangedListener == null) {
                return;
            }

            if (WifiManager.NETWORK_STATE_CHANGED_ACTION.equals(intent.getAction())) {
                NetworkInfo nInfo = intent.getParcelableExtra(WifiManager.EXTRA_NETWORK_INFO);
                if (nInfo.getType() == ConnectivityManager.TYPE_WIFI) {
                    if (nInfo.isConnected() && !nInfo.isFailover()) {
                        WifiInfo wInfo = intent.getParcelableExtra(WifiManager.EXTRA_WIFI_INFO);
                        if (wInfo.getSupplicantState() == SupplicantState.COMPLETED) {
                            Log.d(TAG, "Connection with Wifi " + wInfo.getSSID() + " completed");
                            mStateHandler.sendEmptyMessage(STATE_CONNECTED);
                        }
                    } else if (nInfo.getState() == NetworkInfo.State.DISCONNECTED) {
                        mStateHandler.sendEmptyMessage(STATE_DISCONNECTED);
                    }
                }
            } else if (WifiManager.SCAN_RESULTS_AVAILABLE_ACTION.equals(intent.getAction())) {
                // If we're lost connected or moved to another network, show results.
                mStateHandler.sendEmptyMessage(STATE_WIFI_DISCOVERED);
            }
        }
    };

    private static class WifiHandler extends WeakHandler<WifiHelper> {

        /**
         * Creates a new weak handler of type <T>
         *
         * @param owner The owner object which should be referenced
         */
        WifiHandler(WifiHelper owner, Looper looper) {
            super(owner, looper);
        }

        @Override
        public void handleMessage(Message msg) {
            WifiHelper that = getOwner();

            switch (msg.what) {
                case WIFI_CONNECT: {
                    WifiManager wifiManager = that.getWifiManager();
                    WifiConfiguration wifiConfiguration = (WifiConfiguration) msg.obj;

                    // If the wifi configuration is null, stop here.
                    assert wifiConfiguration != null;
                    String SSID = wifiConfiguration.SSID;

                    Log.d(TAG, "Try to connect to wifi: " + SSID);

                    int id = wifiManager.addNetwork(wifiConfiguration);
                    if (id != -1 &&
                            wifiManager.disconnect() &&
                            wifiManager.enableNetwork(id, true) &&
                            wifiManager.reconnect())
                    {
                        Log.d(TAG, "Connecting to new network " + SSID + "...");
                    } else {
                        Message stateMsg = that.mStateHandler.obtainMessage(STATE_CONNECTION_FAILED, id, 0, SSID);
                        that.mStateHandler.sendMessage(stateMsg);
                    }
                }
                break;
            }
        }
    }

    private static class StateHandler extends WeakHandler<WifiHelper> {

        StateHandler(WifiHelper owner) {
            super(owner, owner.mContext.getMainLooper());
        }

        @Override
        public void handleMessage(Message msg) {
            WifiHelper that = getOwner();

            switch (msg.what) {
                case STATE_CONNECTED:
                    that.mWifiStateChangedListener.onWifiConnected(that.getCurrentWifiSSID());
                    Log.d(TAG, "Successfully connected to wifi " + that.getCurrentWifiSSID());
                    break;
                case STATE_CONNECTION_FAILED:
                    Log.e(TAG, "Failed to connect (id=" + msg.arg1 + ", SSID= " + msg.obj + ")");
                    that.mWifiStateChangedListener.onWifiConnectionFailed((String) msg.obj);
                    break;
                case STATE_DISCONNECTED:
                    that.mWifiStateChangedListener.onWifiDisconnected();
                    break;
                case STATE_WIFI_DISCOVERED:
                    that.mWifiStateChangedListener.onWifiDiscovered(that.getWifiSSIDs());
                    break;
                default:
                    break;
            }
        }
    }

    WifiHelper(Context context, OnWifiStateChangedListener listener) {
        mContext = context;
        mWifiStateChangedListener = listener;

        HandlerThread handlerThread = new HandlerThread(TAG);
        handlerThread.start();
        mHandler = new WifiHandler(this, handlerThread.getLooper());
        mStateHandler = new StateHandler(this);
    }

    boolean start() {
        Log.d(TAG, "Start Wifi helper");

        if (!mIsReceiverRegistered) {
            mIsReceiverRegistered = true;
            // Register state change receiver

            IntentFilter intentFilter = new IntentFilter();
            intentFilter.addAction(WifiManager.NETWORK_STATE_CHANGED_ACTION);
            intentFilter.addAction(WifiManager.SCAN_RESULTS_AVAILABLE_ACTION);

            mContext.registerReceiver(mWifiConnectedReceiver, intentFilter);
        }

        WifiManager wifiManager = getWifiManager();
        return wifiManager.startScan();
    }

    void stop() {
        Log.d(TAG, "Stop Wifi helper");
        if (mIsReceiverRegistered) {
            mContext.unregisterReceiver(mWifiConnectedReceiver);
            mIsReceiverRegistered = false;
        }
    }

    boolean isConnectedToWifi(String SSID) {
        String currentSSID = getCurrentWifiSSID();
        Log.d(TAG, "Current wifi SSID: " + currentSSID);
        return currentSSID != null && currentSSID.equals(SSID);
    }

    List<String> getWifiSSIDs() {
        ArrayList<String> wifiSSIDs = new ArrayList<>();
        WifiManager wifiManager = getWifiManager();
        if (wifiManager != null) {
            List<ScanResult> results = wifiManager.getScanResults();
            if (results != null) {
                for (ScanResult result : results) {
                    String SSID = result.SSID;
                    wifiSSIDs.add(SSID);
                }
            }
        }

        return wifiSSIDs;
    }

    void connectToWifi(String SSID) {
        if (SSID == null) {
            return;
        } else if (isConnectedToWifi(SSID)) {
            // Already connected, do not reconnect.
            mStateHandler.sendEmptyMessage(STATE_CONNECTED);
            return;
        }

        WifiConfiguration wifiConfiguration = getMatchingWifiConfiguration(SSID);

        if (wifiConfiguration == null) {
            Log.i(TAG, "No valid Wifi configuration found. Creating a new one.");
            wifiConfiguration = new WifiConfiguration();
            wifiConfiguration.SSID = SSID;

            // If the user has changed the password, he must set it manually in the system wifi settings.
            wifiConfiguration.preSharedKey = mContext.getString(R.string.wifi_default_password);

            // Use static ip configuration for devices with lollipop and hight to prevent disconnects
            // caused by missing DNS probes.
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                try {
                    setStaticIpConfiguration(
                            getWifiManager(),
                            wifiConfiguration,
                            InetAddress.getByName("192.168.42.254"),
                            24,
                            InetAddress.getByName("192.168.42.1"),
                            new InetAddress[] {
                                    InetAddress.getByName("8.8.8.8"),
                                    InetAddress.getByName("8.8.4.4")
                            }
                    );
                } catch (Exception e) {
                    Log.e(TAG, "Setting static ip configuration failed", e);
                }
            }

        } else {
            Log.i(TAG, "Using existing Wifi configuration.");
        }

        Message m = mHandler.obtainMessage(WIFI_CONNECT, wifiConfiguration);
        m.sendToTarget();
    }

    String getCurrentWifiSSID() {
        WifiManager wifiManager = getWifiManager();
        if (wifiManager != null) {
            // Remove quotation marks from the returned SSID
            return wifiManager.getConnectionInfo().getSSID().replace("\"", "");
        }

        return null;
    }

    boolean isWifiEnabled() {
        WifiManager wifiManager = getWifiManager();
        if (wifiManager != null) {
            return wifiManager.isWifiEnabled();
        }
        return false;
    }

    private WifiConfiguration getMatchingWifiConfiguration(String SSID) {
        WifiManager wifiManager = getWifiManager();
        List<WifiConfiguration> configurations = wifiManager.getConfiguredNetworks();
        if (configurations != null) {
            for (WifiConfiguration configuration : configurations) {
                if (configuration.SSID != null && configuration.SSID.equals("\"" + SSID + "\"")) {
                    return configuration;
                }
            }
        }

        return null;
    }

    private WifiManager getWifiManager() {
        return (WifiManager) mContext.getSystemService(Context.WIFI_SERVICE);
    }

    @SuppressWarnings("unchecked")
    private void setStaticIpConfiguration(
            WifiManager manager,
            WifiConfiguration config,
            InetAddress ipAddress,
            int prefixLength,
            InetAddress gateway,
            InetAddress[] dns) throws
            ClassNotFoundException,
            IllegalAccessException,
            IllegalArgumentException,
            InvocationTargetException,
            NoSuchMethodException,
            NoSuchFieldException,
            InstantiationException
    {
        // First set up IpAssignment to STATIC.
        Object ipAssignment = ReflectionHelper.getEnumValue("android.net.IpConfiguration$IpAssignment", "STATIC");
        ReflectionHelper.callMethod(
                config,
                "setIpAssignment",
                new String[] { "android.net.IpConfiguration$IpAssignment" },
                new Object[] { ipAssignment });

        // Then set properties in StaticIpConfiguration.
        Object staticIpConfig = ReflectionHelper.newInstance("android.net.StaticIpConfiguration");

        Object linkAddress = ReflectionHelper.newInstance(
                "android.net.LinkAddress",
                new Class<?>[] { InetAddress.class, int.class },
                new Object[] { ipAddress, prefixLength });

        ReflectionHelper.setField(staticIpConfig, "ipAddress", linkAddress);
        ReflectionHelper.setField(staticIpConfig, "gateway", gateway);
        ReflectionHelper.getField(staticIpConfig, "dnsServers", ArrayList.class).clear();
        for (InetAddress dn : dns) {
            ReflectionHelper.getField(staticIpConfig, "dnsServers", ArrayList.class).add(dn);
        }

        ReflectionHelper.callMethod(
                config,
                "setStaticIpConfiguration",
                new String[] { "android.net.StaticIpConfiguration" },
                new Object[] { staticIpConfig });

        manager.updateNetwork(config);
        manager.saveConfiguration();
    }
}
