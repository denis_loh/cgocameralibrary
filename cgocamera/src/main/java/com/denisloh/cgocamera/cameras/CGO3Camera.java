package com.denisloh.cgocamera.cameras;

import android.os.Handler;
import android.util.Log;

import com.denisloh.cgocamera.CGOCamera;
import com.denisloh.cgocamera.CameraParameters;
import com.denisloh.cgocamera.utils.CommandException;
import com.denisloh.cgocamera.utils.ParseException;
import com.denisloh.cgocamera.utils.ResponseException;

import org.json.JSONException;
import org.json.JSONObject;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.List;
import java.util.Locale;

public class CGO3Camera extends CGOCamera {

    private static final String TAG = CGO3Camera.class.getSimpleName();

    public static final String NAME = "C-GO3";
    public static final int ID = 104;

    private static final String STREAM_URI = "rtsp://192.168.42.1/live";
    private static final String BASE_URL = "http://192.168.42.1/cgi-bin/cgi";

    private static final SimpleDateFormat DATE_FORMAT = new SimpleDateFormat("yyyy-MM-dd_HH:mm:ss", Locale.US);

    private static final List<VideoMode> VIDEO_MODES = new ArrayList<>();
    static {
        VIDEO_MODES.add(new VideoMode("4096x2160","25p"));
        VIDEO_MODES.add(new VideoMode("4096x2160","24p"));
        VIDEO_MODES.add(new VideoMode("3840x2160","30p"));
        VIDEO_MODES.add(new VideoMode("3840x2160","25p"));
        VIDEO_MODES.add(new VideoMode("3840x2160","24p"));
        VIDEO_MODES.add(new VideoMode("2560x1440","30p"));
        VIDEO_MODES.add(new VideoMode("2560x1440","25p"));
        VIDEO_MODES.add(new VideoMode("2560x1440","24p"));
        VIDEO_MODES.add(new VideoMode("1920x1080","120p"));
        VIDEO_MODES.add(new VideoMode("1920x1080","60p"));
        VIDEO_MODES.add(new VideoMode("1920x1080","50p"));
        VIDEO_MODES.add(new VideoMode("1920x1080","48p"));
        VIDEO_MODES.add(new VideoMode("1920x1080","30p"));
        VIDEO_MODES.add(new VideoMode("1920x1080","25p"));
        VIDEO_MODES.add(new VideoMode("1920x1080","24p"));
    }

    @Override
    public int getId() {
        return ID;
    }

    @Override
    public String getName() {
        return NAME;
    }

    @Override
    public String getLiveStreamUrl() {
        return STREAM_URI;
    }

    @Override
    public boolean supportsCommand(Command command) {
        switch (command) {
            case INIT_CAMERA:
            case GET_FIRMWARE_INFO:
                return true;
            default:
                return false;
        }
    }

    @Override
    public void initCamera(Handler handler) {
        this.performRequest(
                handler,
                Command.INIT_CAMERA,
                "INDEX_PAGE");
    }

    @Override
    public void updateStatus(Handler handler) {
        this.performRequest(
                handler,
                Command.GET_STATUS,
                "GET_STATUS");
    }

    @Override
    public void getFirmwareInfo(Handler handler) {
        this.performRequest(
                handler,
                Command.GET_FIRMWARE_INFO,
                "GET_FW_VERSION");
    }

    @Override
    public void startRecord(Handler handler) {
        this.performRequest(
                handler,
                Command.START_RECORDING,
                "START_RECORD");
    }

    @Override
    public void stopRecord(Handler handler) {
        this.performRequest(
                handler,
                Command.STOP_RECORDING,
                "STOP_RECORD");
    }

    @Override
    public void snapshot(Handler handler) {
        this.performRequest(
                handler,
                Command.SNAPSHOT,
                "TAKE_PHOTO");
    }

    @Override
    public void setExposureValue(Handler handler, double exposureValue) {
        this.performRequest(
                handler,
                Command.SET_EXPOSURE_VALUE,
                "SET_EXPOSURE_VALUE",
                new Parameter("value", Double.toString(exposureValue)));
    }

    @Override
    public void getExposureValue(Handler handler) {
        this.performRequest(
                handler,
                Command.GET_EXPOSURE_VALUE,
                "GET_EXPOSURE_VALUE");
    }

    @Override
    public void setPhotoFormat(Handler handler, PhotoFormat format) {
        this.performRequest(
                handler,
                Command.SET_PHOTO_FORMAT,
                "SET_PHOTO_FORMAT",
                new Parameter("value", format.FormatString));
    }

    @Override
    public void getPhotoFormat(Handler handler) {
        this.performRequest(
                handler,
                Command.GET_PHOTO_FORMAT,
                "GET_PHOTO_FORMAT");
    }

    @Override
    public void resetDefaults(Handler handler) {
        this.performRequest(
                handler,
                Command.RESET_DEFAULT,
                "RESET_DEFAULT");
    }

    @Override
    public void setTime(Handler handler, Date time) {
        this.performRequest(
                handler,
                Command.SET_TIME,
                "SET_TIME",
                new Parameter("time", DATE_FORMAT.format(time)));
    }

    @Override
    public void setCameraMode(Handler handler, CameraMode mode) {
        this.performRequest(
                handler,
                Command.SET_CAMERA_MODE,
                "SET_CAM_MODE",
                new Parameter("mode", mode.ModeString));
    }

    @Override
    public void getCameraMode(Handler handler) {
        this.performRequest(
                handler,
                Command.GET_CAMERA_MODE,
                "GET_CAM_MODE");
    }

    @Override
    public void setAudioSwitch(Handler handler, boolean audioOn) {
        this.performRequest(
                handler,
                Command.SET_AUDIO_SW,
                "SET_AUDIO_SW",
                new Parameter("mode", audioOn ? "1" : "0"));
    }

    @Override
    public void getAudioSwitch(Handler handler) {
        this.performRequest(
                handler,
                Command.GET_AUDIO_SW,
                "GET_AUDIO_SW");
    }

    @Override
    public void setVideoMode(Handler handler, String mode) {
        this.performRequest(
                handler,
                Command.SET_VIDEO_MODE,
                "SET_VIDEO_MODE",
                new Parameter("mode", mode));
    }

    @Override
    public void getVideoMode(Handler handler) {
        this.performRequest(
                handler,
                Command.GET_VIDEO_MODE,
                "GET_VIDEO_MODE");
    }

    @Override
    public List<VideoMode> getVideoModes() {
        return VIDEO_MODES;
    }

    @Override
    public void getRecordTime(Handler handler) {
        this.performRequest(
                handler,
                Command.GET_RECORD_TIME,
                "GET_REC_TIME");
    }

    @Override
    public void setWhiteBalanceMode(Handler handler, WhiteBalanceMode mode) {
        this.performRequest(
                handler,
                Command.SET_WHITE_BALANCE_MODE,
                "SET_WHITEBLANCE_MODE",
                new Parameter("mode", Integer.toString(mode.Id)));
    }

    @Override
    public void getWhiteBalanceMode(Handler handler) {
        this.performRequest(
                handler,
                Command.GET_WHITE_BALANCE_MODE,
                "GET_WHITEBLANCE_MODE");
    }

    @Override
    public void setImageQualityMode(Handler handler, ImageQualityMode mode) {
        this.performRequest(
                handler,
                Command.SET_IMAGE_QUALITY_TYPE,
                "SET_IQ_TYPE",
                new Parameter("mode", Integer.toString(mode.Id)));
    }

    @Override
    public void getImageQualityMode(Handler handler) {
        this.performRequest(
                handler,
                Command.GET_IMAGE_QUALITY_TYPE,
                "GET_IQ_TYPE");
    }

    @Override
    public void setShutterTime(Handler handler, int shutterTime) {
        String ISO = getCurrentStatus().PhotoSettings.getISOValue();

        this.performRequest(
                handler,
                Command.SET_SHUTTER_TIME,
                "SET_SH_TM_ISO",
                new Parameter("time", Integer.toString(shutterTime)),
                new Parameter("value", ISO));
    }

    @Override
    public void getShutterTime(Handler handler) {
        this.performRequest(
                handler,
                Command.GET_SHUTTER_TIME,
                "GET_SH_TM_ISO");
    }

    @Override
    public void setISOSensitivity(Handler handler, String ISO) {
        int shutterTime = getCurrentStatus().PhotoSettings.getShutterTime();

        this.performRequest(
                handler,
                Command.SET_ISO_SENSITIVITY,
                "SET_SH_TM_ISO",
                new Parameter("time", Integer.toString(shutterTime)),
                new Parameter("value", ISO));
    }

    @Override
    public void getISOSensitivity(Handler handler) {
        this.performRequest(
                handler,
                Command.GET_ISO_SENSITIVITY,
                "GET_SH_TM_ISO");
    }

    @Override
    public void setAutoExposure(Handler handler, boolean aeEnabled) {
        this.performRequest(
                handler,
                Command.SET_AUTO_EXPOSURE_ENABLE,
                "SET_AE_ENABLE",
                new Parameter("mode", aeEnabled ? "1" : "0"));
    }

    @Override
    public void getAutoExposure(Handler handler) {
        this.performRequest(
                handler,
                Command.GET_AUTO_EXPOSURE_ENABLE,
                "GET_AE_ENABLE");
    }

    @Override
    public void formatSDCard(Handler handler) {
        this.performRequest(
                handler,
                Command.FORMAT_SDCARD,
                "FORMAT_CARD");
    }

    @Override
    public void getSDCardTotalSpace(Handler handler) {
        this.performRequest(
                handler,
                Command.GET_SDCARD_TOTAL_SPACE,
                "GET_SPACE");
    }

    @Override
    public void getSDCardFreeSpace(Handler handler) {
        this.performRequest(
                handler,
                Command.GET_SDCARD_FREE_SPACE,
                "GET_SPACE_FREE");
    }

    @Override
    protected Object parseResponse(Command command, String response) throws ParseException, CommandException {
        Log.d(TAG, "Response was " + response);

        Object data = null;

        if (response != null && !response.isEmpty()) {
            try {
                JSONObject object = new JSONObject(response);
                int status = object.optInt("rval", ERROR_PARSE_FAILED);
                if (status == 0) {
                    CameraParameters.PhotoSettings photoSettings = getCurrentStatus().PhotoSettings;

                    switch (command) {
                        case INIT_CAMERA:
                            // Fall through
                        case GET_STATUS:
                            data = parseStatus(object);
                            break;
                        case GET_FIRMWARE_INFO:
                            data = parseFirmware(object);
                            break;
                        case START_RECORDING:
                            getCurrentStatus().setStatus(StatusMode.RECORDING);
                            break;
                        case STOP_RECORDING:
                            getCurrentStatus().setStatus(StatusMode.VIEWFINDER);
                            break;
                        case SNAPSHOT:
                            // no parsing;
                            break;
                        case SET_EXPOSURE_VALUE:
                            // no parsing;
                            break;
                        case GET_EXPOSURE_VALUE:
                            double ev = object.getDouble("exposure_value");
                            photoSettings.setExposureValue(ev);
                            data = photoSettings;
                            break;
                        case SET_PHOTO_FORMAT:
                            // no parsing;
                            break;
                        case GET_PHOTO_FORMAT:
                            PhotoFormat format = PhotoFormat.fromString(object.getString("photo_format"));
                            photoSettings.setPhotoFormat(format);
                            data = photoSettings;
                            break;
                        case SET_WHITE_BALANCE_MODE:
                            // no parsing;
                            break;
                        case GET_WHITE_BALANCE_MODE:
                            WhiteBalanceMode wbMode = WhiteBalanceMode.fromInt(object.getInt("wb_mode"));
                            photoSettings.setIsAutoWhiteBalanceLocked(wbMode == WhiteBalanceMode.LOCK);
                            photoSettings.setWhiteBalance(wbMode);
                            data = photoSettings;
                            break;
                        case SET_IMAGE_QUALITY_TYPE:
                            break;
                        case GET_IMAGE_QUALITY_TYPE:
                            ImageQualityMode iqType = ImageQualityMode.fromInt(object.getInt("IQ_type"));
                            photoSettings.setImageQualityType(iqType);
                            data = photoSettings;
                            break;
                        case SET_SHUTTER_TIME:
                        case SET_ISO_SENSITIVITY:
                            // no parsing;
                            break;
                        case GET_SHUTTER_TIME:
                        case GET_ISO_SENSITIVITY:
                            int shutter = object.getInt("shutter_time");
                            String iso = object.getString("iso_value");
                            photoSettings.setShutterTime(shutter);
                            photoSettings.setISOValue(iso);
                            data = photoSettings;
                            break;
                        case SET_SHARPNESS:
                            // no parsing;
                            break;
                        case GET_SHARPNESS:
                            int sharpness = object.getInt("sharpness");
                            photoSettings.setSharpness(sharpness);
                            data = photoSettings;
                            break;
                        case SET_AUTO_EXPOSURE_ENABLE:
                            // no parsing;
                            break;
                        case GET_AUTO_EXPOSURE_ENABLE:
                            boolean aeEnable = object.getInt("ae_enable") != 0;
                            photoSettings.setIsAutoExposureEnabled(aeEnable);
                            data = photoSettings;
                            break;
                        case SET_CAMERA_MODE:
                            // no parsing;
                            break;
                        case GET_CAMERA_MODE:
                            CameraMode mode = CameraMode.fromInt(object.getInt("cam_mode"));
                            getCurrentStatus().setCameraMode(mode);
                            data = mode;
                            break;
                        case SET_AUDIO_SW:
                            // no parsing;
                            break;
                        case GET_AUDIO_SW:
                            boolean audioSwitch = object.getInt("audio_sw") != 0;
                            getCurrentStatus().setIsAudioEnabled(audioSwitch);
                            data = audioSwitch;
                            break;
                        case SET_VIDEO_MODE:
                            // no parsing;
                            break;
                        case GET_VIDEO_MODE:
                            String videoMode = object.getString("video_mode");
                            getCurrentStatus().setVideoMode(videoMode);
                            data = videoMode;
                            break;
                        case GET_RECORD_TIME:
                            int recordTime = object.getInt("param");
                            getCurrentStatus().setRecordTime(recordTime);
                            data = recordTime;
                            break;
                        case RESET_DEFAULT:
                            // no parsing;
                            break;
                        case SET_TIME:
                            // no parsing;
                            break;
                        case FORMAT_SDCARD:
                            // no parsing;
                            break;
                        case GET_SDCARD_TOTAL_SPACE:
                            int totalSpace = object.getInt("param");
                            getCurrentStatus().setSDCardTotalMemory(totalSpace);
                            data = totalSpace;
                            break;
                        case GET_SDCARD_FREE_SPACE:
                            int freeSpace = object.getInt("param");
                            getCurrentStatus().setSDCardFreeMemory(freeSpace);
                            data = freeSpace;
                            break;
                        default:
                            // Maybe the super class can parse this command.
                            data = super.parseResponse(command, response);
                            break;
                    }
                } else {
                    String message = object.optString("msg", "Camera error: " + status);
                    throw new ResponseException(command, status, message);
                }
            } catch (JSONException e) {
                throw new ParseException(e);
            }
        }

        return data;
    }

    private void performRequest(Handler handler, Command command, String commandString, Parameter... params) {
        ArrayList<Parameter> commandParameters = new ArrayList<>(Arrays.asList(params));
        commandParameters.add(new Parameter("CMD", commandString));

        performRequest(handler, command, commandParameters);
    }

    private void performRequest(Handler handler, Command command, List<Parameter> params) {
        super.performRequest(handler, BASE_URL, command, params);
    }

    private CameraParameters parseStatus(JSONObject object) throws JSONException, CommandException {
        CameraParameters parameters = getCurrentStatus();

        if (object.has("fw_ver")) parameters.Firmware.setVersion(object.getString("fw_ver"));

        if (object.has("status")) parameters.setStatus(StatusMode.fromString(object.getString("status")));
        if (object.has("cam_mode")) parameters.setCameraMode(CameraMode.fromInt(object.getInt("cam_mode")));
        if (object.has("speed_rate")) parameters.setSpeedRate(object.getString("speed_rate"));

        if (object.has("ae_enable")) parameters.PhotoSettings.setIsAutoExposureEnabled(object.getInt("ae_enable") != 0);
        if (object.has("iq_type")) parameters.PhotoSettings.setImageQualityType(ImageQualityMode.fromInt(object.getInt("iq_type")));
        if (object.has("white_balance")) parameters.PhotoSettings.setWhiteBalance(WhiteBalanceMode.fromInt(object.getInt("white_balance")));
        if (object.has("awb_lock")) parameters.PhotoSettings.setIsAutoWhiteBalanceLocked(object.getInt("awb_lock") != 0);
        if (object.has("sharpness")) parameters.PhotoSettings.setSharpness(object.getInt("sharpness"));
        if (object.has("shutter_time")) parameters.PhotoSettings.setShutterTime(object.getInt("shutter_time"));
        if (object.has("iso_value")) parameters.PhotoSettings.setISOValue(object.getString("iso_value"));

        if (object.has("video_mode")) parameters.setVideoMode(object.getString("video_mode"));
        if (object.has("audio_sw")) parameters.setIsAudioEnabled(object.getInt("audio_sw") != 0);
        if (object.has("record_time")) parameters.setRecordTime(object.getInt("record_time"));

        if (object.has("sdfree")) parameters.setSDCardFreeMemory(object.getInt("sdfree"));
        if (object.has("sdtotal")) parameters.setSDCardTotalMemory(object.getInt("sdtotal"));

        return parameters;
    }

    private CameraParameters.FirmwareInfo parseFirmware(JSONObject object) throws JSONException, CommandException {
        CameraParameters.FirmwareInfo firmwareInfo = getCurrentStatus().Firmware;

        if (object.has("brand")) firmwareInfo.setBrand(object.getString("brand"));
        if (object.has("model")) firmwareInfo.setModel(object.getString("model"));
        if (object.has("YUNEEC_ver")) firmwareInfo.setVersion(object.getString("YUNEEC_ver"));
        if (object.has("api_ver")) firmwareInfo.setApiVersion(object.getString("api_ver"));
        if (object.has("fw_ver")) firmwareInfo.setVersionDate(object.getString("fw_ver"));
        if (object.has("chip")) firmwareInfo.setSensorChip(object.getString("chip"));

        return firmwareInfo;
    }
}
