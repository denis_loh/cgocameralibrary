package com.denisloh.cgocamera.cameras;

import android.os.Handler;

import com.denisloh.cgocamera.CGOCamera;
import com.denisloh.cgocamera.CameraParameters;

/**
 * MK58 Digital Downlink for GoPro
 *
 * This camera is used by the MK58 Digital Downlink for
 * GoPro Hero 3/3+/4 Action Cams.
 *
 * This camera only supports live streaming at the moment.
 */
public class MK58Camera extends CGOCamera {

    /**
     * Name of the Camera
     */
    public static final String NAME = "MK58/GoPro";

    /**
     * Camera ID
     */
    public static final int ID = 105;

    /**
     * RTSP Streaming url of the downlink
     */
    private static final String STREAM_URI = "rtsp://192.168.110.1/cam1/h264";

    @Override
    public int getId() {
        return ID;
    }

    @Override
    public String getName() {
        return NAME;
    }

    @Override
    public String getLiveStreamUrl() {
        return STREAM_URI;
    }

    @Override
    public boolean supportsCommand(Command command) {
        switch (command) {
            case INIT_CAMERA:
            case GET_FIRMWARE_INFO:
                return true;
            default:
                return false;
        }
    }

    @Override
    public void getFirmwareInfo(Handler handler) {
        performLocalRequest(handler, Command.GET_FIRMWARE_INFO, NO_ERROR, createFirmware());
    }

    private CameraParameters.FirmwareInfo createFirmware() {
        CameraParameters.FirmwareInfo firmwareInfo = getCurrentStatus().Firmware;

        firmwareInfo.setBrand("Yuneec");
        firmwareInfo.setModel(getName());

        return firmwareInfo;
    }
}
