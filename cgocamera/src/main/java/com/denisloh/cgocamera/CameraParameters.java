package com.denisloh.cgocamera;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Camera parameters
 *
 * These parameters contain the current status of the camera.
 */
public class CameraParameters implements Parcelable {

    public static class FirmwareInfo implements Parcelable {

        /**
         * Forbid new instances
         */
        private FirmwareInfo() {}

        /**
         * Brand of the camera
         *
         * This is the name of the camera producer or retailer
         *
         * For instance: Yuneec
         */
        private String mBrand;

        /**
         * Name of the camera model
         *
         * This is the device name of the camera
         *
         * For instance: CGO3
         */
        private String mModel;

        /**
         * Firmware version of the camera
         *
         * This is a string representation of the firmware version.
         *
         * For instance: 1.8.00(E)
         */
        private String mVersion;

        /**
         * API version
         *
         * This is a string representation of the API version.
         *
         * For instance: 2.8.00
         */
        private String mApiVersion;

        /**
         * Release date of the firmware version
         *
         * This is the date when this firmware version was released.
         *
         * For instance: Jul 17 2015 14:02:17
         */
        private String mVersionDate;

        /**
         * Sensor chip of the camera
         *
         * This is a string representation of the used sensor chip in the camera.
         *
         * For instance: S2(A9)
         */
        private String mSensorChip;

        protected FirmwareInfo(Parcel in) {
            mBrand = in.readString();
            mModel = in.readString();
            mVersion = in.readString();
            mApiVersion = in.readString();
            mVersionDate = in.readString();
            mSensorChip = in.readString();
        }

        public static final Creator<FirmwareInfo> CREATOR = new Creator<FirmwareInfo>() {
            @Override
            public FirmwareInfo createFromParcel(Parcel in) {
                return new FirmwareInfo(in);
            }

            @Override
            public FirmwareInfo[] newArray(int size) {
                return new FirmwareInfo[size];
            }
        };

        public String getBrand() {
            return mBrand;
        }

        public void setBrand(String mBrand) {
            this.mBrand = mBrand;
        }

        public String getModel() {
            return mModel;
        }

        public void setModel(String mModel) {
            this.mModel = mModel;
        }

        public String getVersion() {
            return mVersion;
        }

        public void setVersion(String mFirmwareVersion) {
            this.mVersion = mFirmwareVersion;
        }

        public String getApiVersion() {
            return mApiVersion;
        }

        public void setApiVersion(String mApiVersion) {
            this.mApiVersion = mApiVersion;
        }

        public String getVersionDate() {
            return mVersionDate;
        }

        public void setVersionDate(String mVersionDate) {
            this.mVersionDate = mVersionDate;
        }

        public String getSensorChip() {
            return mSensorChip;
        }

        public void setSensorChip(String mSensorChip) {
            this.mSensorChip = mSensorChip;
        }

        @Override
        public int describeContents() {
            return 0;
        }

        @Override
        public void writeToParcel(Parcel dest, int flags) {
            dest.writeString(mBrand);
            dest.writeString(mModel);
            dest.writeString(mVersion);
            dest.writeString(mApiVersion);
            dest.writeString(mVersionDate);
            dest.writeString(mSensorChip);
        }
    }

    public static class PhotoSettings implements Parcelable {

        /**
         * Forbid new instances
         */
        private PhotoSettings() {}

        /**
         * State if auto exposure is enabled
         *
         * This gives the current state if Auto Exposure (AE) is enabled or not.
         *
         * <i>1</i> it AE is enabled, <i>0</i> otherwise
         */
        private boolean mIsAutoExposureEnabled;

        /**
         * Current image quality (IQ) type
         *
         * This is an integer value of predefined IQ settings.
         *
         * Valid values are:
         * - 0: Natural
         * - 1: Gorgeous
         * - 2: raw
         * - 3: night
         */
        private CGOCamera.ImageQualityMode mImageQualityType;

        /**
         * Current white balance (WB) type
         *
         * This is an integer value of predefined WB settings.
         *
         * Valid values are:
         * - 0: Automatic White Balance
         * - 1: Incandescent
         * - 3: Sunset
         * - 4: Daylight
         * - 5: Cloudy
         * - 7: Fluorescent
         * - 10: Custom
         * - 99: White Balance Lock
         */
        private CGOCamera.WhiteBalanceMode mWhiteBalance;

        /**
         * State if Auto White Balance (AWB) is locked
         *
         * <i>1</i> it AWB lock is enabled, <i>0</i> otherwise
         */
        private boolean mIsAutoWhiteBalanceLocked;

        /**
         * The sharpness of the jpg image
         *
         * Valid values range from 0 - very soft - to 10 - very sharp.
         */
        private int mSharpness;

        /**
         * Current shutter time value
         *
         * This is an integer representation of the shutter time in 1/x seconds
         *
         * Valid values are for instance: 30, 60, 250, 1000, 4000, 8000
         */
        private int mShutterTime;

        /**
         * Current exposure value (EV)
         *
         * This is a double representation of the exposure value.
         *
         * Valid values ranges from -2.0 to 2.0
         */
        private double mExposureValue;

        /**
         * Current ISO value
         *
         * This is a string representation of the ISO value.
         *
         * Valid values are for instance:
         * - 100
         * - 200
         * - 1600
         * - 3200
         */
        private String mISOValue;

        /**
         * Current photo format
         *
         * This is a string representation of the photo format.
         *
         * Valid values are:
         * - jpg: JPEG Images
         * - dng: RAW Images
         */
        private CGOCamera.PhotoFormat mPhotoFormat;

        protected PhotoSettings(Parcel in) {
            mIsAutoExposureEnabled = in.readInt() != 0;
            mImageQualityType = CGOCamera.ImageQualityMode.fromInt(in.readInt());
            mWhiteBalance = CGOCamera.WhiteBalanceMode.fromInt(in.readInt());
            mIsAutoWhiteBalanceLocked = in.readInt() != 0;
            mSharpness = in.readInt();
            mShutterTime = in.readInt();
            mExposureValue = in.readDouble();
            mISOValue = in.readString();
            mPhotoFormat = CGOCamera.PhotoFormat.valueOf(in.readString());
        }

        public static final Creator<PhotoSettings> CREATOR = new Creator<PhotoSettings>() {
            @Override
            public PhotoSettings createFromParcel(Parcel in) {
                return new PhotoSettings(in);
            }

            @Override
            public PhotoSettings[] newArray(int size) {
                return new PhotoSettings[size];
            }
        };

        public boolean isIsAutoExposureEnabled() {
            return mIsAutoExposureEnabled;
        }

        public void setIsAutoExposureEnabled(boolean mIsAutoExposureEnabled) {
            this.mIsAutoExposureEnabled = mIsAutoExposureEnabled;
        }

        public CGOCamera.ImageQualityMode getImageQualityType() {
            return mImageQualityType;
        }

        public void setImageQualityType(CGOCamera.ImageQualityMode mImageQualityType) {
            this.mImageQualityType = mImageQualityType;
        }

        public CGOCamera.WhiteBalanceMode getWhiteBalance() {
            return mWhiteBalance;
        }

        public void setWhiteBalance(CGOCamera.WhiteBalanceMode mWhiteBalance) {
            this.mWhiteBalance = mWhiteBalance;
        }

        public boolean isIsAutoWhiteBalanceLocked() {
            return mIsAutoWhiteBalanceLocked;
        }

        public void setIsAutoWhiteBalanceLocked(boolean mIsAutoWhiteBalanceLocked) {
            this.mIsAutoWhiteBalanceLocked = mIsAutoWhiteBalanceLocked;
        }

        public int getSharpness() {
            return mSharpness;
        }

        public void setSharpness(int mSharpness) {
            this.mSharpness = mSharpness;
        }

        public int getShutterTime() {
            return mShutterTime;
        }

        public void setShutterTime(int mShutterTime) {
            this.mShutterTime = mShutterTime;
        }

        public double getExposureValue() {
            return mExposureValue;
        }

        public void setExposureValue(double mExposureValue) {
            this.mExposureValue = mExposureValue;
        }

        public String getISOValue() {
            return mISOValue;
        }

        public void setISOValue(String mISOValue) {
            this.mISOValue = mISOValue;
        }

        public CGOCamera.PhotoFormat getPhotoFormat() {
            return mPhotoFormat;
        }

        public void setPhotoFormat(CGOCamera.PhotoFormat mPhotoFormat) {
            this.mPhotoFormat = mPhotoFormat;
        }

        @Override
        public int describeContents() {
            return 0;
        }

        @Override
        public void writeToParcel(Parcel dest, int flags) {
            dest.writeInt(mIsAutoExposureEnabled ? 1 : 0);
            dest.writeInt(mImageQualityType.Id);
            dest.writeInt(mWhiteBalance.Id);
            dest.writeInt(mIsAutoWhiteBalanceLocked ? 1 : 0);
            dest.writeInt(mSharpness);
            dest.writeInt(mShutterTime);
            dest.writeDouble(mExposureValue);
            dest.writeString(mISOValue);
            dest.writeString(mPhotoFormat.name());
        }
    }

    public final FirmwareInfo Firmware;

    public final PhotoSettings PhotoSettings;

    /**
     * Current status of the camera
     *
     * Valid values are:
     * - vf: view finder
     * - record: Recording in video function
     */
    private CGOCamera.StatusMode mStatus;

    /**
     * Current mode of the camera
     */
    private CGOCamera.CameraMode mCameraMode;

    /**
     * Speed rate of the camera wifi
     *
     * This is a string representation of the speed rate in Megabit per Second
     *
     * For instance: 6M for 6MBit/s
     */
    private String mSpeedRate;

    /**
     * Current video mode
     *
     * This is a string representation of the video mode containing
     * the video resolution and the frame rate
     *
     * Valid values are for instance:
     * - 1920x1080F24
     * - 1920x1080F120
     * - 4096x2160F25
     */
    private String mVideoMode;

    /**
     * State if audio is enabled during recording
     *
     * <i>1</i> it audio is enabled, <i>0</i> otherwise
     */
    private int mIsAudioEnabled;

    /**
     * Record time in seconds
     *
     * This is a integer representation of the record time in seconds.
     */
    private int mRecordTime;

    /**
     * Free memory of the inserted SD card
     *
     * This is the free memory of the SD card in kilobytes
     */
    private long mSDCardFreeMemory;

    /**
     * Total memory of the inserted SD card
     *
     * This is the total memory of the SD card in kilobytes
     */
    private long mSDCardTotalMemory;

    CameraParameters() {
        Firmware = new FirmwareInfo();
        PhotoSettings = new PhotoSettings();

        mStatus = CGOCamera.StatusMode.VIEWFINDER;
        mCameraMode = CGOCamera.CameraMode.VIDEO;
        mSpeedRate = "";

        mVideoMode = "4096x2160F25";
        mIsAudioEnabled = 0;
        mRecordTime = 0;

        mSDCardFreeMemory = 0;
        mSDCardTotalMemory = 0;
    }

    protected CameraParameters(Parcel in) {
        Firmware = in.readParcelable(FirmwareInfo.class.getClassLoader());
        PhotoSettings = in.readParcelable(PhotoSettings.class.getClassLoader());

        mStatus = CGOCamera.StatusMode.valueOf(in.readString());
        mCameraMode = CGOCamera.CameraMode.valueOf(in.readString());
        mSpeedRate = in.readString();

        mVideoMode = in.readString();
        mIsAudioEnabled = in.readInt();
        mRecordTime = in.readInt();

        mSDCardFreeMemory = in.readLong();
        mSDCardTotalMemory = in.readLong();
    }

    public static final Creator<CameraParameters> CREATOR = new Creator<CameraParameters>() {
        @Override
        public CameraParameters createFromParcel(Parcel in) {
            return new CameraParameters(in);
        }

        @Override
        public CameraParameters[] newArray(int size) {
            return new CameraParameters[size];
        }
    };

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeParcelable(Firmware, flags);

        dest.writeString(mStatus.name());
        dest.writeString(mCameraMode.name());
        dest.writeString(mSpeedRate);

        dest.writeString(mVideoMode);
        dest.writeInt(mIsAudioEnabled);
        dest.writeInt(mRecordTime);

        dest.writeLong(mSDCardFreeMemory);
        dest.writeLong(mSDCardTotalMemory);
    }

    public CGOCamera.StatusMode getStatus() {
        return mStatus;
    }

    public void setStatus(CGOCamera.StatusMode mStatus) {
        this.mStatus = mStatus;
    }

    public boolean isRecording() {
        return mStatus == CGOCamera.StatusMode.RECORDING;
    }

    public CGOCamera.CameraMode getCameraMode() {
        return mCameraMode;
    }

    public void setCameraMode(CGOCamera.CameraMode mCameraMode) {
        this.mCameraMode = mCameraMode;
    }

    public String getSpeedRate() {
        return mSpeedRate;
    }

    public void setSpeedRate(String mSpeedRate) {
        this.mSpeedRate = mSpeedRate;
    }

    public String getVideoMode() {
        return mVideoMode;
    }

    public void setVideoMode(String mVideoMode) {
        this.mVideoMode = mVideoMode;
    }

    public boolean isIsAudioEnabled() {
        return mIsAudioEnabled == 1;
    }

    public void setIsAudioEnabled(boolean mIsAudioEnabled) {
        this.mIsAudioEnabled = mIsAudioEnabled ? 1 : 0;
    }

    public int getRecordTime() {
        return mRecordTime;
    }

    public void setRecordTime(int mRecordTime) {
        this.mRecordTime = mRecordTime;
    }

    public long getSDCardFreeMemory() {
        return mSDCardFreeMemory;
    }

    public void setSDCardFreeMemory(long mSDCardFreeMemory) {
        this.mSDCardFreeMemory = mSDCardFreeMemory;
    }

    public long getSDCardTotalMemory() {
        return mSDCardTotalMemory;
    }

    public void setSDCardTotalMemory(long mSDCardTotalMemory) {
        this.mSDCardTotalMemory = mSDCardTotalMemory;
    }
}
