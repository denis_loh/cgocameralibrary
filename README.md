# README #

Master: [![Build Status](https://www.bitrise.io/app/dfbeed53553cff08.svg?token=KYGbgac66F3iz5ucF2Fg0w&branch=master)](https://www.bitrise.io/app/dfbeed53553cff08) Development: [![Build Status](https://www.bitrise.io/app/dfbeed53553cff08.svg?token=KYGbgac66F3iz5ucF2Fg0w&branch=dev-0.2.0)](https://www.bitrise.io/app/dfbeed53553cff08) [![Download](https://api.bintray.com/packages/denis-loh/maven/CGOCamera/images/download.svg) ](https://bintray.com/denis-loh/maven/CGOCamera/_latestVersion)


This is an unofficial android library and SDK for the Yuneec Typhoon Q500 copter cameras. The library currently supports the following cameras:

- CGO2+
- CGO3
- MK58 Digital Downlink (a.k.a GoPro or GB203)

Other cameras may follow.

# PURPOSE #

The purpose of the library is to enable you and everyone who is interested in creating custom apps to control the camera of the Yuneec Q500. I created this library primarily to add some additional features like intervalometer, timelapse, VR.

## GRADLE ##

Use the following maven repository in your projects build.gradle

```
repositories {
    maven {
        url  "http://dl.bintray.com/denis-loh/maven"
    }
}
```

and add this dependency to your app build.gradle

```
compile 'com.denisloh.cgocamera:CGOCamera:0.2.0'
```

## DEPENDENCIES TO OTHER LIBRARIES ##

This library depends on the following third party libraries:

```
com.mcxiaoke.volley:library:1.0.19
```

# BASIC INFORMATION #

The cameras of the yuneec copters provide their own 5.2/5.8 GHz Wifi. You can connect to this Wifi with almost every smartphone or tablet which is capable of 801.11 n/ac with 5GHz. They also provide an integrated DHCP, which assigns an IP to you device. Afterwards, you can view the live video stream with a video player app (i.e. VLC). Additionally, you can download the files from the camera without removing the SD card. Use the following URIs:

## CGO2+/CGO3 ##
* Media files: http://192.168.42.1/DCIM/100MEDIA/
* Live stream: rtsp://192.168.42.1/live

## MK58/GoPro ##
* Live stream: rtsp://192.168.110.1/cam1/h264

## Lumix/GCO4 ##
* Live stream: rtsp://192.168.73.254:8556/PSIA/Streaming/channels/2?videoCodecType=H.264

# LICENCE #

This library is released under the terms of the Apache 2 licence. Feel free to use the library for your projects, but give me a note if you do so to give you some credits on this site.